#Miguel Correia

"""
Dice rolling sim
"""
import random
x="yes"
choice=input("What type of dice do you wish to roll?\nDice with 6 numbers or 20? ")
if choice=="20":
    while x=="Yes" or x=="yes":
        d=random.randint(1,20)
        print(d) 
        x=input("Do you wish to roll again? ")
elif choice=="6":
    while x=="Yes" or x=="yes":
        choice2=input("Do you wish to roll one or two dice? ")
        if choice2=="one" or choice2=="1":
            d=random.randint(1,6)
            print(d)
        elif choice2=="two" or choice2=="2":
            d1=random.randint(1,6)
            d2=random.randint(1,6)
            print(d1+d2)
        else:
            print("Choose a valid choice.")
        x=input("Do you wish to roll again? ")
    print("Ok! See you!")
else:
    print("Choose a valid choice.")