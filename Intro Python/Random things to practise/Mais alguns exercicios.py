# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 09:49:33 2020

@author: Miguel Correia
"""

#Exercicio distancia entre 2 pontos, tamanho de uma linha

import math
def line_length(P1,P2):
    val=((P2[0]-P1[0])**2)+((P2[1]-P1[1])**2)
    dist=math.sqrt(val)
    return "{:.2f}".format(dist)
print(line_length([15,7],[22,11]))


#Exercicio verificar quantos 1's,3's e 9's cabem num número

class ones_three_nines:
    def __init__(self,num):
        self.num=num
    def ones(self):
        ones=self.num
        return ones
    def threes(self):
        threes=self.num//3
        return threes
    def nines(self):
        nines=self.num//9
        return nines
n1=ones_three_nines(10)
print(n1.nines())
print(n1.threes())
print(n1.ones())


#Exercicio transforma string para morse

def string_morse(string):
    char_to_dots = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.',
        'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..',
        'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.',
        'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-',
        'Y': '-.--', 'Z': '--..', ' ': ' ', '0': '-----',
        '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....',
        '6': '-....', '7': '--...', '8': '---..', '9': '----.',
        '&': '.-...', "'": '.----.', '@': '.--.-.', ')': '-.--.-', '(': '-.--.',
        ':': '---...', ',': '--..--', '=': '-...-', '!': '-.-.--', '.': '.-.-.-',
        '-': '-....-', '+': '.-.-.', '"': '.-..-.', '?': '..--..', '/': '-..-.'
        }
    morse_string=""
    string=string.strip(" ")
    string=list(string.strip())
    for char in string:
        char=char.capitalize()
        if char==string[(len(string)-1)]:
            char=char_to_dots[char]
            morse_string+=char
        else:
            char=char_to_dots[char]
            morse_string=morse_string+char+" "
    print(morse_string)
string_morse("hello world")


"""
Create a function that takes a variable number of arguments, each 
argument representing the number of items in a group, and returns the 
number of permutations (combinations) of items that you could get by 
taking one item from each group.

Notes
Don't overthink this one.
Input may include the number zero.
"""

def combinations(*items):
    total=1
    zero=False
    for item in items:
        if item>0:
            total*=item
        else:
            zero=True
    if zero==True:
        print("One or more of the groups have no items.")
    else:
        pass
    if total==1:
        total=0
    else:
        pass
    return total
print(combinations(2,3))
print(combinations(3,7,4))
print(combinations(0,2,3))


"""
Write a function that returns the greatest common divisor of all list 
elements. If the greatest common divisor is 1, return 1.

Notes
List elements are always greater than 0.
There is a minimum of two list elements given.
"""
import math
def GCD(n_list):
    n_list.sort()
    if len(n_list)==1:
        gcd=n_list[0]
    elif len(n_list)==2:
        gcd=math.gcd(n_list[0],n_list[1])
    else:
        gcd=n_list[0]
        for i in range(len(n_list)-1):
            gcd=math.gcd(gcd,n_list[(i)])
    return gcd
print(GCD([11,7,5,2,4]))


"""
You're able to call numbers like 1-800-flowers which replace the 
characters with the associated numbers on a cellular device keyboard.

This is your task:

-Create a function that takes a string as argument.
-Convert all letters to numbers by using a cellular device keyboard 
as reference and leave any other characters in.
-Return a string containing the argument with replaced letters.
"""

data={"abc":2,"def":3,"ghi":4,"jkl":5,"mno":6,"pqrs":7,"tuv":8,
      "wxyz":9}
times={"a":1,"b":2,"c":3,"d":1,"e":2,"f":3,"g":1,"h":2,"i":3,"j":1,
       "k":2,"l":3,"m":1,"n":2,"o":3,"p":1,"q":2,"r":3,"s":4,"t":1,
       "u":2,"v":3,"w":1,"x":2,"y":3,"z":4}
choice=input("Is it a number or a text? ")
choice=choice.lower()
if choice=="number":
    number=input("Insert a phone number:")
    new_number=""
    for char in number:
        char=char.lower()
        for i in data.items():
            if char in i[0]:
                new_number+=str(i[1])
                break
            else:
                continue
        else:
            new_number+=str(char)
    print(new_number)
elif choice=="text":
    number=input("Insert the text:")
    new_number=""
    for char in number:
        char=char.lower()
        for i in data.items():
            if char in i[0]:
                new_number+=str(i[1])*times[char]
                break
            else:
                continue
        else:
            new_number+=str(char)
    print(new_number)
else:
    print("Insert a valid choice.")
    
    
"""
Create a function that takes a list of numbers or strings and returns 
a list with the items from the original list stored into sublists. 
Items of the same value should be in the same sublist.

advanced_sort([2, 1, 2, 1]) ➞ [[2, 2], [1, 1]]

advanced_sort([5, 4, 5, 5, 4, 3]) ➞ [[5, 5, 5], [4, 4], [3]]

advanced_sort(["b", "a", "b", "a", "c"]) ➞ [["b", "b"], ["a", "a"], ["c"]]

The sublists should be returned in the order of each element's first 
appearance in the given list.
"""
def advanced_sort(unsorted_list):
    lists_list=[]
    for item in unsorted_list:
        if len(lists_list)!=0:
            for lists in lists_list:
                if item not in lists:
                    is_item_list=False
                else:
                    lists.append(item)
                    is_item_list=True
                    break
            if is_item_list==False:
                new_l=[item]
                lists_list.append(new_l)
        else:
            new_l=[item]
            lists_list.append(new_l)
    return lists_list
print(advanced_sort([1,1,5,3,1,2,5]))


"""
Given a 3x3 matrix of a completed tic-tac-toe game, create a function 
that returns whether the game is a win for "X", "O", or a "Draw", 
where "X" and "O" represent themselves on the matrix, and "E" 
represents an empty spot.

tic_tac_toe([
  ["X", "O", "X"],
  ["O", "X",  "O"],
  ["O", "X",  "X"]
]) ➞ "X"

tic_tac_toe([
  ["O", "O", "O"],
  ["O", "X", "X"],
  ["E", "X", "X"]
]) ➞ "O"

tic_tac_toe([
  ["X", "X", "O"],
  ["O", "O", "X"],
  ["X", "X", "O"]
]) ➞ "Draw"

Make sure that if O wins, you return the letter "O" and not the 
integer 0 (zero) and if it's a draw, make sure you return the 
capitalised word "Draw". If you return "X" or "O", make sure they're 
capitalised too.
"""

















