#@author: Miguel Correia

"""
you have a function called random that generates a number from 0 to 1
randomly and it's uniformly distributed
pi*r^2/(2*r)^2=circle_points/total_points
area of circle/area of square
area of square = (2)^2
r=1, so...
pi/(2)^2=circle_points/total_points
pi=4*circle_points/total_points
"""
import random
import math
def randnum(n):
    circle_points=0
    total_points=0
    for num in range(n):
        x=random.random()
        y=random.random()
        squaresum=x**2+y**2
        distance=math.sqrt((squaresum))
        if distance<=1:
            circle_points=circle_points+1
            total_points=total_points+1
        else:
            total_points=total_points+1
    return circle_points,total_points

circle_points,total_points=randnum(611000) #The bigger this number is
                                 #the more accurate it is
pi=4*circle_points/total_points