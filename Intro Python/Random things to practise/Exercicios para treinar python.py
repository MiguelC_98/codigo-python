# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 10:44:17 2020

@author: migue
"""

#Exercicio que cria uma lista de numeros.
def inclusive_list(start_num,end_num):
    inc_list=[]
    for i in range(start_num,end_num+1):
        inc_list.append(i)
    return inc_list
print(inclusive_list(2,10))

#Exercicio de diferença de areas entre um quadrado dentro de um circulo,
#e um um quadrado com o mesmo circulo dentro dele

import math
def square_areas_difference(r):
    #l de grande quadrado igual a 2r
    l_big=2*r
    #teorema de pitagoras resolve o pequeno
    l_small=math.sqrt((r**2+r**2))
    big_area=l_big**2
    small_area=l_small**2
    area_dif=big_area-small_area
    return round(area_dif)
print(square_areas_difference(6))

#Exercicio que faz a equaçao quadratica

import math
def quad_eq(a,b,c):
    l=[]
    sqrtx=((b**2)-(4*a*c))
    if sqrtx<0:
        return "Não existem resultados reais!"
    else:
        sqrt=math.sqrt(sqrtx)
    try:
        x=(-b+(sqrt))/(2*a)
        l.append(x)
    except:
        pass
    try:
        negx=(-b-(sqrt))/(2*a)
        l.append(negx)
    except:
        pass
    if len(l)==2:
        return round(l[0]),round(l[1])
    elif len(l)==1:
        return round(l[0])
    else:
        return "Sem resultado!"
print(quad_eq(1,4,2))

#Exercicio Lista de Multiplos

def mult_list(num,end):
    list_mult=[]
    for i in range(1,end+1):
        mult=num*i
        list_mult.append(mult)
    return list_mult
print(mult_list(12,10))


#Exercicio class Calculadora

class Calculadora:
    def add(self,num1,num2):
        return num1+num2
    def subtract(self,num1,num2):
        return num1-num2
    def multiply(self,num1,num2):
        return num1*num2
    def divide(self,num1,num2):
        return num1/num2
calc=Calculadora()
print(calc.multiply(1,2))

#CALCULADORA COM TKINTER

import tkinter
expression=""
def press(num):
    global expression
    expression=expression+str(num)
    equation.set(expression)

def equalpress():
    try:
        global expression
        total=str(eval(expression))
        equation.set(total)
        expression=""
    except:
        equation.set("Error")
        expression=""

def clear():
    global expression
    expression=""
    equation.set(expression)

if __name__ == "__main__":
    gui=tkinter.Tk()
    gui.configure(background="light gray")
    gui.title("Simple Calculator")
    gui.geometry("300x180")
    equation=tkinter.StringVar()
    expression_field=tkinter.Entry(gui,textvariable=equation)
    expression_field.grid(columnspan=4,ipadx=70)
    equation.set("Enter your expression")
    button1=tkinter.Button(gui,text="1",fg="black",bg="light blue",
                           command=lambda: press(1),height=1,width=7)
    button1.grid(row=2,column=0)
    button2=tkinter.Button(gui,text="2",fg="black",bg="light blue",
                           command=lambda: press(2),height=1,width=7)
    button2.grid(row=2,column=1)
    button3=tkinter.Button(gui,text="3",fg="black",bg="light blue",
                           command=lambda: press(3),height=1,width=7)
    button3.grid(row=2,column=2)
    button4=tkinter.Button(gui,text="4",fg="black",bg="light blue",
                           command=lambda: press(4),height=1,width=7)
    button4.grid(row=3,column=0)
    button5=tkinter.Button(gui,text="5",fg="black",bg="light blue",
                           command=lambda: press(5),height=1,width=7)
    button5.grid(row=3,column=1)
    button6=tkinter.Button(gui,text="6",fg="black",bg="light blue",
                           command=lambda: press(6),height=1,width=7)
    button6.grid(row=3,column=2)
    button7=tkinter.Button(gui,text="7",fg="black",bg="light blue",
                           command=lambda: press(7),height=1,width=7)
    button7.grid(row=4,column=0)
    button8=tkinter.Button(gui,text="8",fg="black",bg="light blue",
                           command=lambda: press(8),height=1,width=7)
    button8.grid(row=4,column=1)
    button9=tkinter.Button(gui,text="9",fg="black",bg="light blue",
                           command=lambda: press(9),height=1,width=7)
    button9.grid(row=4,column=2)
    button0=tkinter.Button(gui,text="0",fg="black",bg="light blue",
                           command=lambda: press(1),height=1,width=7)
    button0.grid(row=5,column=0)
    plus=tkinter.Button(gui,text="+",fg="black",bg="light blue",
                           command=lambda: press("+"),height=1,width=7)
    plus.grid(row=2,column=3)
    minus=tkinter.Button(gui,text="-",fg="black",bg="light blue",
                           command=lambda: press("-"),height=1,width=7)
    minus.grid(row=3,column=3)
    mult=tkinter.Button(gui,text="*",fg="black",bg="light blue",
                           command=lambda: press("*"),height=1,width=7)
    mult.grid(row=4,column=3)
    div=tkinter.Button(gui,text="/",fg="black",bg="light blue",
                           command=lambda: press("/"),height=1,width=7)
    div.grid(row=5,column=3)
    clear=tkinter.Button(gui,text="Clear",fg="black",bg="light blue",
                           command=clear,height=1,width=7)
    clear.grid(row=5,column=1)
    equal=tkinter.Button(gui,text="=",fg="black",bg="light blue",
                           command=equalpress,height=1,width=7)
    equal.grid(row=5,column=2)
    dec=tkinter.Button(gui,text=".",fg="black",bg="light blue",
                           command=lambda: press("."),height=1,width=7)
    dec.grid(row=6,column=0)
    gui.mainloop()



















