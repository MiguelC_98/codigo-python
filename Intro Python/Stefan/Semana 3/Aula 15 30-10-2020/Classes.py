# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 10:32:39 2020

@author: Miguel Correia
"""

"""
https://www.notion.so/a3cbf31503ce4ed6b8ffd36c5a366c22?v=cc80cd0325814d6eb10417f9c883d896&p=6e2ac341bca549778ef627d84479fa70
Para definir uma class é só necessário:
    class Myclass:
        area=120
        window_size=(2,3) #propriedades da classe
        colour="blue"
        
        def func(self): #funções da classe
            pass
print(Myclass.area) #120
            

-Variáveis que são definidas ao nivel da classe são variáveis da classe.

-Classes também podem ter funções, essas funções têm de ter sempre pelo
menos um argumento, "self", que referencia o objecto.

-Variáveis e funções dentro de uma classe são chamadas por atributos.

-As funções da classe só podem ser acedidas por um objeto da classe a
menos que se consiga passar um objeto.

-Para aceder a uma propriedade da classe utiliza-se "."

-Um inicializador é como a função int() que cria um objeto da classe int
,para criares um objeto na tua classe só tens de indicar o nome da 
classe e usar parentesis entre o objeto, como por exemplo:
    Myclass(<objeto>)
Ao fazeres isso, dás os atributos da tua classe, ao objeto.

-Para criares o teu inicializador tens de criar uma função __init__
dentro da classe.

-O __init__ personaliza o layout do objeto ou personaliza os seus
atributos
"""

class Myclass:
    area=120
    window_size=(2,3)
    
    def abre_porta(self):
        print("Abriu a porta")
        
    def __init__(self,cor_das_paredes=0x000000,cor_do_telhado=0x0000ff):
        self.cor_das_paredes=cor_das_paredes
        self.cor_do_telhado=cor_do_telhado

print(Myclass.area) 
print(Myclass.abre_porta) 
concrete_house=Myclass(0xffffff,0x0000ff) #0x indica que é hexadecimal
other_house=Myclass()
concrete_house.area=450
print(Myclass.area)        #120
print(concrete_house.area) #450
print(concrete_house.cor_das_paredes)
print(concrete_house.cor_do_telhado)
print(other_house.cor_das_paredes)
print(other_house.cor_do_telhado)
another_house=Myclass()
another_house.portas=2 #adiciona-se o atributo de portas com o valor de 2
print(another_house.portas) #2 
del(another_house.portas) #apaga o atributo das portas do objeto mas
#apaga o atributo da classe se estiver definido na classe por isso... 
#é melhor NÃO UTILIZAR O del

"""
Inheritence é quando uma classe contem todos os atributos e funções
que uma classe tem mas queremos ainda mais do que a classe tem.

Para definir que uma class herda outra usa-se:
"""
class Human:
    compassion=True
class Pais:
    def __init__(self,nome,idade):
        self.nome=nome
        self.idade=idade
    def trabalhar(self):
        pass
    

class Eu(Pais,Human):
    def __init__(self,nome,idade,hobby):
        super().__init__(nome,idade)
        self.hobby=hobby
    def trabalhar(self):
        super.trabalhar()
        print("e trabalhar mais")

eu=Eu("Miguel",21,"Jogar")
print("O meu nome é",eu.nome,"e tenho",eu.idade,"anos, e o meu hobby é",eu.hobby)
"""
Ao usar a função super está-se a ir buscar certos atributos á super 
classe (A classe cujos atributos foste buscar), isto funciona melhor
para chamar os atributos numa função.
"""
print({eu.trabalhar})
print("I have compassion",{eu.compassion})
        




















