# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 08:38:58 2020

@author: Miguel Correia
"""
"""

Exercicio 1
a. map(lambda x:x+1,range(1,4)) #[2,3,4]
b. map(lambda x:x>0,[3,-5,-2,0]) #[3]
c. filter(lambda x:x>5,range(1,7)) #[6]
d. filter(lambda x:x%2==0,range(1,11)) #[2,4,6,8,10]

Exercicio 2
a.reduce(lambda y,z:y*3+z, range(1,5))-(((1*3+2)*3+3)*3)+4=58
b.reduce(lambda x,y:x**2+y,range(2,6))-(((2**2+3)**2+4)**2+5)=2814
"""
