# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 09:18:05 2020

@author: Miguel Correia
"""

#Exercicio 1
"""
1.
out_file=open("outFile.txt","w")
for i in range(100):
    out_file.write(str(i))

-Escreve os números de 0 a 99 num ficheiro de texto

2.
in_file=open("inFile.txt","r")
indata=in_file.read()
in_file.close()
indata=in_file.read()
in_file.close()

-Abre o ficheiro "inFile", depois guarda a leitura do ficheiro em uma
variável, fechando o ficheiro depois disso, causando um erro quando se
tenta ler o ficheiro de novo apesar de não estar aberto.

3.
in_file = open(“inFile.txt”,'r')
print(in_file.readline())
in_file = open(“inFile.txt” ,'r'))
print(in_file.readline())
in_file.close()

-Abre o ficheiro, faz print a primeira linha do ficheiro, logo após
reabre o ficheiro de novo, voltando ao inicio, e fazendo print a primeira
linha de novo 
"""
#Exercicio 2

with open("Example file.txt") as text:
    for x in text:
        print(x)

#Exercicio 3
i=1
with open("Example file.txt") as text:
    for x in text:
        print(i,x)
        i+=1

#Exercicio 4
def medias(ficheiro):
    with open(ficheiro,"r") as all_temps:
        for line in all_temps:
            l=line.split(" ")
            for i in range(len(l)):
                l[i]=float(l[i])
            #tambem funciona float_list=list(map(float,l))
            media=(sum(l)/len(l))
            print(line,"{:.2f}".format(media))
try:
    medias("temp.txt")
except FileNotFoundError:
    print("Ficheiro não foi encontrado.")

#Exercicio 5
def temps():
    nome_ficheiro=input("Insira o nome do seu ficheiro: ")
    try:
        medias(nome_ficheiro)
    except FileNotFoundError:
        print("Erro de I/O ao ler o ficheiro: ficheiro não foi encontrado.")
temps()

#Exercicio 6
def linha_para_elementos(elementos):
    dados=elementos.split(" ")
    dic_dados={"nome":dados[0],"atomico":int(dados[1]),"densidade":float(dados[2])}
    return dic_dados
with open("C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Stefan\\Semana 3\\Aula 14 29-10-2020\\simbolos quimicos.txt","r",encoding="utf-8") as quimica:
    h=linha_para_elementos(quimica.readline())
    #ou se quiseres todos os elementos...
    todos_elementos=[]
    for elementos in quimica:
        todos_elementos.append(linha_para_elementos(elementos))

#Exercicio 7
def escrever_elementos(nome_ficheiro,todos_elementos):
    with open(nome_ficheiro,"w") as ficheiro:
        for elemento in todos_elementos:
            ficheiro.write(elemento["nome"]+" ")
            ficheiro.write(str(elemento["atomico"])+" ")
            ficheiro.write(str(elemento["densidade"])+"\n")
nome_ficheiro=input("Insira um nome para o ficheiro: ")
nome_ficheiro=nome_ficheiro + ".txt"
escrever_elementos(nome_ficheiro,todos_elementos)

#Exercicio 8
nome_ficheiro="C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Stefan\\Semana 3\\Aula 14 29-10-2020\\moleculas.txt"
def linha_para_atomo(linha):
    linha_lista=linha.split(" ")
    if "ATOM" in linha_lista[0]:
        atom={"símbolo":linha_lista[2],"id":linha_lista[1],
              "x":linha_lista[3],"y":linha_lista[4],"z":linha_lista[5]}
        return atom
    else:
        pass
        
with open(nome_ficheiro,"r") as ficheiro:
    todas_as_linhas=ficheiro.readlines()
    valor_do_atomo=linha_para_atomo(todas_as_linhas[3])


#MANIPULAÇÃO DE CSV******************************MANIPULAÇÃO DE CSV
import csv
with open('eggs.csv', 'rU') as ficheiro_csv:
    leitor = csv.reader(ficheiro_csv, delimiter=' ', quotechar="|")
    for linha in leitor:
        print(",".join(linha))
    print(leitor.__next__()) #lê uma linha de cada vez

#Exercicio 1
import csv
grafico=[]
def csvLinhaParaGrafico():
    with open('Gráficos.csv', 'rU') as ficheiro_csv:
        leitor = csv.reader(ficheiro_csv, delimiter=';')
        valores_x=leitor.__next__()
        valores_y=leitor.__next__()
        for i in range(len(valores_x)):
            valores_x[i]=valores_x[i].replace(",",".")
            valores_y[i]=valores_y[i].replace(",",".")
            grafico.append((float(valores_x[i]),float(valores_y[i])))
        return grafico
csvLinhaParaGrafico()
#Outra maneira******************Outra maneira
def coverte_para_notacao(item1,item2):
    return float(item1.replace(",",".")),float(item2.replace(",","."))
def le_graficos(ficheiro_csv):
    grafico=[]
    with open(ficheiro_csv) as csvfile:
        csv_reader=csv.reader(csvfile,delimiter=";")
        linha1=csv_reader.__next__()
        linha2=csv_reader.__next__()
        grafico = list(map(coverte_para_notacao,linha1,linha2))
        return grafico
print(le_graficos("Gráficos.csv)"))


#Exercicio 2

import csv
def graficoParaCsvLinha(nome_ficheiro,grafico):
    with open(nome_ficheiro,"w",newline="") as ficheiro_csv:
        escritor=csv.writer(ficheiro_csv)
        escritor.writerow(grafico[0])
        escritor.writerow(grafico[1])
nome_ficheiro=input("Insira um nome para o ficheiro: ")
nome_ficheiro=nome_ficheiro+".csv"
grafico=[[2,5,3,4],[1,2,5,3]]
graficoParaCsvLinha(nome_ficheiro,grafico)


#Exercicio 3

import csv
def graficoParaCsv(nome_ficheiro,lista_graficos):
    with open(nome_ficheiro,"w",newline="") as ficheiro:
        x=False
        escritor=csv.writer(ficheiro)
        for graficos in lista_graficos:
            if x==False:
                escritor.writerow(graficos[0])
                x=True
            else:
                pass
            escritor.writerow(graficos[1])
lista_graficos=[[[1,2],[3,4]],[[1,2],[5,6]],[[1,2],[7,8]]]
nome_ficheiro=input("Insira um nome para o ficheiro: ")
nome_ficheiro=nome_ficheiro+".csv"
graficoParaCsv(nome_ficheiro,lista_graficos)













