# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 10:50:54 2020

@author: Miguel Correia
"""

f=open("blobfile.txt")
#abre o ficheiro em modo "r"(modo de leitura)
#f=open("blobfile.txt", "x")
#cria o ficheiro, se já existir resulta num erro
# f=open("blobfile.txt", "w")
#abre o ficheiro em modo de escrita, cria um ficheiro se não existir
# f=open("blobfile.txt","a")
#abre o ficheiro para juntar a outro, cria um ficheiro se não existir


# print(f.read()) #lê o ficheiro todo, não funcionando para ler a seguir
print(f.readline()) #lê uma linha
for line in f:
    print(line)
    #igual a f.readline() mas para o ficheiro todo
f.close() #é sempre necessário fechar o ficheiro

with open("blobfile.txt","r",encoding="utf-8") as file_reader:
    #ao escrever assim, quando a identação acaba deste bloco o python
    #fecha o ficheiro, não sendo necessário usar o close()
    for line in file_reader:
        print(line)
f.closed #verifica se o ficheiro está aberto, returnando um boolean


f=open("blobfile.txt","a") #adiciona o seguinte texto no final
f.write("\nOh my god o ficheiro tem mais texto! Yay!")
f.close()
f=open("blobfile.txt")
print(f.read())
f.close()

f=open("blobfile.txt", "w") #irá escrever por cima
f.write("Tou farto de adicionar texto :(")
f.close()
f=open("blobfile.txt")
print(f.read())
f.close()

"""
import os
os.remove("blobfile.txt") #Para remover ficheiros
"""
import os
print(os.path.abspath("blobfile.txt")) #mostra o absolute path
print(os.path.relpath("blobfile.txt")) #mostra o relative path
print(os.path.basename("./blobfile.txt")) #mostra o verdadeiro nome do ficheiro

with open("blobfile.txt") as file_reader:
    lines=file_reader.readlines()
    print(lines)
with open("blobfile.txt","w") as file_writer:
    file_writer.writelines(lines)
with open("blobfile.txt") as file_reader:
    lines_after_writing=file_reader.readlines()
    print(lines_after_writing==lines)


#Erros e excepções-----------------------------------Erros e Excepções
try:
    int("2"/0)
except ValueError:
    print("The string must contain numbers, not letters.")
except:
    print("How did you get here? This is the secret level!")
finally:
    print("thnx user, you're really useful")

with open("blobfile.txt") as file_reader:
    try:
        file_reader.readlines()
        #do something
    except ValueError as identifier:
        identifier.bla() #assim podemos pegar na exceção e modifica-la
    














