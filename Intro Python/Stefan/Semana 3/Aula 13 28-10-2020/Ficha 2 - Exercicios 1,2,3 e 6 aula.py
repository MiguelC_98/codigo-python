# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 11:21:47 2020

@author: Miguel Correia
"""

#Exercicio 1- Considere a seguinte função:
def imprimeDivisaoInteira(x,y):
    if y==0:
        print("Divisão por zero")
    else:
        print(x//y)
"""
a) O que faz esta função: Esta função divide dois números e arredonda 
para baixo

b)Qual o resultado da seguinte sequência de comandos?
    imprimeDivisaoInteira(4,2)-2
    imprimeDivisaoInteira(2,4)-0
    imprimeDivisaoInteira(3,0)-"Divisão por zero"
    help(imprimeDivisaoInteira)-imprimeDivisaoInteira(x,y)
    imprimeDivisaoInteira()-Error
c)Altere a definição da função de modo a adotar a abordagem -...
"""

def imprimeDivisaoInteira(x,y):
    """

    Parameters
    ----------
    x : int
        Dividendo>0.
    y : int
        Divisor>0.

    Returns
    -------
    None.

    """
    print(x//y)
#Exercicio 2- Considere a seguinte função:
def potencia(a,b):
    return a**b
"""
a)O que faz a seguinte sequência de comandos?
a=2
b=3
potencia(b,a)-9
potencia(a,b)-8
print(potencia(b,a)) -prints 9
print(potencia(a,b)) -prints 8
print(potencia(2,0)) - prints 1
print(potencia(2)) -Erro

b)Escreva uma nova função potenciaP que receba apenas um número inteiro
a e returne a^a
"""
def potenciaP(a):
    return a**a
print(potenciaP(3))
"""
c)Escreva uma nova versão da função potencia que possa ser chamada com
dois ou com um argumento e, consoante o caso, execute a^0 ou a^a
"""
def potencia(a,b=None):
    if b==None:
        b=a
    else:
        pass
    return a**b
print(potencia(2))
print(potencia(2,4))


#Exercicio 3- Considere o seguinte programa
a=4
def printFuncao():
    a=17
    print("Dentro da função:",a)
printFuncao()
print("Fora da função:",a)
"""
a)Qual é o resultado de executar este programa?
    "Dentro da função: 17"
    "Fora da função: 4"

b)O que é uma variável global? E uma variável local?
    Uma variável globlal é uma variável definida dentro do módulo,
    isto é, uma variável que pode ser chamada em qualquer lado dentro
    do módulo.
    Uma variável local é uma variável definida dentro de uma função
    ou classe, só podendo ser utilizada dentro da mesma.
"""
#Exercicio 6-considere o seguinte programa
DIA_ATUAL=2
MES_ATUAL=11
ANO_ATUAL=2015
print("Dados do Pai")
anoPai=int(input("Introduza o ano do nascimento: "))
mesPai=int(input("Introduza o mês do nascimento: "))
diaPai=int(input("Introduza o dia do nascimento: "))
print("Dados da Mãe")
anoMae=int(input("Introduza o ano do nascimento: "))
mesMae=int(input("Introduza o mês do nascimento: "))
diaMae=int(input("Introduza o dia do nascimento: "))

if mesPai>MES_ATUAL or (mesPai==MES_ATUAL and diaPai>DIA_ATUAL):
    print("Pai tem",ANO_ATUAL-anoPai-1,"anos")
else:
    print("Pai tem",ANO_ATUAL-anoPai,"anos")
if mesMae>MES_ATUAL or (mesMae==MES_ATUAL and diaMae>DIA_ATUAL):
    print("Mãe tem",ANO_ATUAL-anoMae-1,"anos")
else:
    print("Mãe tem",ANO_ATUAL-anoMae,"anos")
"""
Recorrendo a funções simplifique o programa, eliminando a repetição
de código
"""
DIA_ATUAL=2
MES_ATUAL=11
ANO_ATUAL=2015
def dados(parente):
    if parente=="Pai":
        print("Dados do Pai")
    else:
        print("Dados da Mãe")
    ano=int(input("Introduza o ano do nascimento: "))
    mes=int(input("Introduza o mês do nascimento: "))
    dia=int(input("Introduza o dia do nascimento: "))
    return (ano, mes, dia, parente)
def anos_atuais(ano,mes,dia,parente):
    if mes>MES_ATUAL or (mes==MES_ATUAL and dia>DIA_ATUAL):
        print(parente,"tem",ANO_ATUAL-ano-1,"anos")
    else:
        print(parente,"tem",ANO_ATUAL-ano,"anos")
dadosPai=dados("Pai")
dadosMae=dados("Mãe")
anos_atuais(dadosPai[0],dadosPai[1],dadosPai[2],dadosPai[3])
anos_atuais(dadosMae[0],dadosMae[1],dadosMae[2],dadosMae[3])






















