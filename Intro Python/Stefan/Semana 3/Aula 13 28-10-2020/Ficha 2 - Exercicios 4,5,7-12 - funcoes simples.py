# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 16:40:41 2020

@author: Miguel Correia
"""

#Exercicio 4


def somaDivisores(num):
    """
  Soma de divisores de um numero dado
  Requires:
  num seja int e num > 0
  Ensures: um int correspondente à soma dos divisores
  de num que sejam maiores que 1 e menores que num
  """
    if num>0 and type(num)==int:
        div=[]
        for i in range(2,num):
            if (num/i)==(num//i):
                div.append(i)
            else:
                continue    
        if num not in div:
            div.append(num)
        else:
            pass
        return sum(div)
    else:
        print("Insert a valid number please.")

"""
4.a.-É necessário inserir um número inteiro e maior que zero para a
função funcionar.

4.b.-Obtém-se a soma dos divisores todos do número.
"""
#Exercicio 5
def força_divi(num=1):
    while num>=0:
        print("Se quiserer para insira um número negativo.")
        print(somaDivisores(num))
        num=int(input("Insira um número: "))
força_divi()
#Exercicio 7
def num_maior(num1,num2):
    """

    Parameters
    ----------
    num1 : int, has to be different than num2
    num2 : int, has to be different than num1

    Returns
    -------
    The largest number between num1, and num2

    """
    return max(num1,num2)
def num_menor(num1,num2):
    m_num=num_maior(num1,num2)
    if num1!=m_num:
        return num1
    else:
        return num2
num1=int(input("Insira um número inteiro: "))
num2=int(input("Insira outro número inteiro: "))
print("O número maior é",num_maior(num1,num2))
print("O número menor é",num_menor(num1,num2))

#Exercicio 8

def unidades(num):
    """
    Também funciona se mudarmos a função para só // por 10, e se for
    menos do que 10, returna 0

    Parameters
    ----------
    num : int
        número inteiro maior que zero.

    Returns
    -------
    o número sem o último número, ou se for só um único número
    returnar zero.

    """
    if num<10:
        return 0
    else:
        num=str(num)
        num=num[:-1]
        num=int(num)
        return num
print(unidades(534))

#Exercicio 9

def zero(num):
    """

    Parameters
    ----------
    num : int
        número inteiro.

    Returns
    -------
    num com um zero adicionado no fim.

    """
    return num*10
print(zero(12))

#Exercicio 10

def somaDivisores(num):
    """
    Soma de divisores de um numero dado
    Requires:
    num seja int e num > 0
    Ensures: um int correspondente à soma dos divisores
    de num que sejam maiores que 1 e menores que num
  
    """
    if num>0 and type(num)==int:
        div=[]
        for i in range(1,num):
            if (num/i)==(num//i):
                div.append(i)
            else:
                continue    
        return sum(div)
    else:
        print("Insira um número válido.")
num=int(input("Insira um número inteiro: "))
if somaDivisores(num)!=None:
    print("A soma dos seus divisores é",somaDivisores(num))
else:
    pass

#Exercicio 11
def num_prime(num):
    if num>0 and type(num)==int:
        for i in range(2,num+1):
            if (num//i)==(num/i) and i!=num:
                break
            elif (num//i)==1:
                return num
            else:
                continue
    else:
        print("Insert a valid number please.")
num=int(input("Insert a number: "))
x=num_prime(num)
if x!=None:
    print(x,"is a prime number")
else:
    print(num,"is not a prime number")

#Exercicio 12
def quantidade_num_prime(n):
    l=[]
    for x in range(2,n+1):
        if num_prime(x)!=None:
            l.append(num_prime(n))
        else:
            continue
    return len(l)
n=int(input("Insira um número inteiro: "))
print("Existem",quantidade_num_prime(n),"números primos entre 2 e",n)
























