#Miguel Correia

"""
Functions:
    It's a block of code that recieves certain inputs, does something
    with those inputs, and returns, or does, something. You can always
    use functions again, all you have to do is call them back.

In Python there are actually 3 different blocks of code, functions,
procedures and generators. A function always has a return, a procedure
is simply a block of code that changes the state of a program but doesn't
return anything, a generator is a block of code that returns different
values as they are needed.

A function is python always needs to start with the syntax:
  def <name of the function>(<obligatory parameter>,<optional parameter>):

The obligatory parameters are something that always needs to be there
when you call for the function, if you don't have it there you'll get an
error. The optional parameter does not make an error if you call the
function and you don't input it but it won't change the value at all.
"""
def multiplication(num,value=2):
    return num*value

"""
If num=2 then multiplication will return 4, if you call for the 
function as "multiplication(2,3)" it will return 6. That shows that
the value is the optional parameter, because if you don't associate a
value to it, it'll take the value of 2, unlike the num, which is the 
obligatory parameter, that -needs- a value

Functions parameters can be integers, lists, even other functions! Those
that recieve other functions as parameters are called high-level functions,
for example sort() is a high-level function, because it can receive two
parameters, reverse, which is a true or false boolean, and key, which is
a function.
"""
a=[["a",2],["a",0],["b",25],["c",0]]
a.sort()
def compare_items(item):
    return len(item[0])
a=[["a",2],["vadvssfdsaa",0],["ewavoidfsadsahsab",25],["«ldsahcc",0]]
a.sort(key=compare_items) #compares items by length

"""
Functions can not not only have other functions as a parameter, but they
can have functions within functions. You can also call a function within
it's own function, it's called -recursion-, for example:
"""
def print_numbers(start,end):
	if start == end: # break condition
		return
	else: # suite
		print(start)
		print_numbers(start+1,end)
print_numbers(2,5)
"""
This function is as if you have a for i in range(2,5): loop, printing
2,3,4 just like the for loop would.
Python programs have a limit on recursions to not crash your computer.
When you do the recursion a lot of times and it goes overboard it is
called stack overflow.

The print function doesn't return anything, it simply writes something
to the interpreter's memory and the interpreter shows it on the screen,
therefore it's a procedure.
"""
a="c"
def triple(string):
    string=string*3 #???? I'm confused.
triple(a)
print()
"""
The range() function is an example for a generator function, returning
elements as they are needed, from it's defined start to end.
Yield will save the value of the variable in the function without
ending the function. 
"""
def range(start, end, step=1):
	i = start
	while i < end:
		yield i
		i += 1
for i in range(0,2):
    print(i)
"""
This shows that the range() function can be a generator function
(it's actually a class but that'll be our secret shhhh)

There are three high-level function built-in python that are essential
to knowing, they are:
    -map(), it apllies a function to each element of the list. It 
    basically substitutes this function:
"""
def function(lista,func):
    for i in range(len(lista)):
        lista[i]=func(lista[i])
    return lista
def add_two(x):
    return x+2
lista=[2,1,3,4]
function(lista,add_two)
print(lista)
lista=[2,1,3,4]
a=list(map(add_two,lista))
print(a)
"""
    -filter(),filters the elements of a list, given a certain function
    it returns a boolean, True or False.
"""
a=[1,12,33,2]
def it_dezasseis(x):
    return x<16
filtered_a=list(filter(it_dezasseis,a))
print(filtered_a)
"""    
    -sorted(),can sort a tuple, which the regular sort() can't, and 
    lists, and you can put a key or reverse it if you like.
"""
a=("abc","dfhoiasfdishaodh","j","iodvhs")
sorted_a=list(sorted(a,key=len))
print(sorted_a)
"""
There is also a function called "reduce" which reduces the data 
structure to one element by aplying a certain function to each element.
It is a part of the functools import.
"""
def add (a,b):
    return a+b
from functools import reduce
lista_listas=[1,2,3,4,5]
sumed_lista=reduce(add,lista_listas)
print(sumed_lista)
"""
We basically created the sum() function in the next function.
"""
a=["abc","def","ghi"]
def my_sum(lista):
    accumulator = type(lista[0])() #Creates a blank of the certain type
    for item in lista:
        accumulator+=item
    return accumulator
sumed_a=my_sum(a)
print(sumed_a)

#Other reduce example
from functools import reduce
def add (a,b):
    return a+b
fruits=["banana","apple","peach"]
sumed_fruits=reduce(add,fruits,"I love") #I lovebananaapplepeach
print(sumed_fruits)
#Or you can use...
sumed_fruits=reduce(lambda x,y:x + y, fruits) #bananaapplepeach
print(sumed_fruits)
"""
lambda indicated that we're defining a nameless function and using it
temporarly
Here we see list compressing, a way easier and faster way of adding
values to a list.
"""
listax=[1,2,3]
listax2=[x+2 for x in listax] #[3,4,5]
"""
And here we see what's called, list flattening. Here we transform lists
inside a list transform into only one list, it's as if we have two 
for loops in only one line of code
"""
a=[[1,2],[3,4]]
flattened_a=[sub_item for item in a for sub_item in item]
print(flattened_a)#[1, 2, 3, 4]
#Another way with reduce
from functools import reduce
flattened_a=reduce(lambda x,y: x + y, a)
print(flattened_a)



























