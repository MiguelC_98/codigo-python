#Miguel Correia

"""
Write a program that reads an hour in hours, minutes and seconds, and it
translates it to seconds
"""
#With datetime
from datetime import datetime
hour=datetime.now().hour
minutes=datetime.now().minute
seconds=datetime.now().second
minutes=minutes+(60*hour)
seconds=seconds+(60*minutes)
print("We are currently",seconds,"seconds into the day")

#Without datetime
h=input("Insert the current hours, minutes and seconds: ")
h_comp=h.split(":")
tot_secs=int(h_comp[0])*60*60+int(h_comp[1])*60+int(h_comp[2])
print(tot_secs)