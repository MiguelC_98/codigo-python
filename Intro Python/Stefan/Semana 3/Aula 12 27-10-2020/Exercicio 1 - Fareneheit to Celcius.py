#Miguel Correia

"""
Write in python a  program that reads a temperature in ºF and transforms
to ºC
"""

F=float(input("Insert your Farenheit temperature: "))
C=(F-32)/1.8
print("Your temperature in Celcius is: {:.2f}".format(C))