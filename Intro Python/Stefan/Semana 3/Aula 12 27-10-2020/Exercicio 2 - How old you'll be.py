#Miguel Correia

"""
Write a python that asks the user for his year of birth, and then tell 
him how old he'll be at the end of the year
"""
import datetime
user_year=int(input("What year were you born? "))
current_year=datetime.datetime.now().year
age=current_year-user_year
print("You'll be {} years old by the end of the year".format(age))