#Miguel Correia

"""
Write a program in python that recieves an integer number and represents
it as a roman number.
"""
def romanvalue(number):
    val = [
        1000, 900, 500, 400,
        100, 90, 50, 40,
        10, 9, 5, 4,
        1
        ]
    syb = [
        "M", "CM", "D", "CD",
        "C", "XC", "L", "XL",
        "X", "IX", "V", "IV",
        "I"
        ]
    roman_num = ''
    i = 0
    while  number > 0: #Enquanto o numero for maior que 0, adiciona 1 ao i
        for _ in range(number // val[i]): #quando o nr e divisivel...
            roman_num += syb[i] #adiciona o correspondente para a string
            number -= val[i] #retira o valor adicionado e retoma o loop
        i += 1               #ate o numero = 0, o roman esta feito ai.
    return roman_num
        
number=abs(int(input("Insert a number: ")))
print(romanvalue(number))

#Feito por mim
num=abs(int(input("Insert a number: ")))
r_dict={1000:"M",900:"CM",500:"D",400:"CD",100:"C",
        90:"XC",50:"L",40:"XL",10:"X",9:"IX",5:"V",
        4:"IV",1:"I"}
val = [
        1000, 900, 500, 400,
        100, 90, 50, 40,
        10, 9, 5, 4,
        1
        ]
roman_num=""
i=0
for x in val:
    quociente=num//x
    if quociente!=0:
        for _ in range(quociente):
            roman_num+=r_dict[x]
            num-=val[i]
        else:
            pass
    i+=1
print(roman_num)
















