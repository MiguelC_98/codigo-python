# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 14:39:39 2020

@author: Miguel Correia
"""
#Exercicio 4
t=9999
h=t//3600
m=(t-(h*3600))//60
s=t-(h*3600+m*60)
print("São",h,"horas",m,"minutos e",s,"segundos.")

# Exercicio 9
"""
x=1
y=1
while x=1 and y<5 #Missing another = in x, so x would be x==1, and a : 
    y=y+2
"""

#Exercicio 10
n=int(input("Escreva um número inteiro: "))
print("Tabuada do",n,":")
i=1
while i<=10:
    print(n,"x",i,"=",n*i)
    i=i+1
"""
No final do programa faltava escrever que era o i que era igual a i+1.
Assim corrigido o valor do i no final do programa é 11, porque ainda
imprime o n*10, depois logo adiciona +1 para o i, fazendo o i=11, e volta
para o while, que repara que o i é maior do que 10, parando o loop.
E o valor de n continua igual ao valor que o utilizador escolheu, pois
nunca é mudado no programa, só usado para multiplicação.
"""

#Exercicio 11
dividendo = int(input("Dividendo: "))
divisor = int(input("Divisor: "))
resto = dividendo
quociente = 0
while resto >= divisor:
    resto = resto - divisor
    quociente = quociente + 1 
    print("O quociente é", quociente, "e o resto é", resto)
"""
a.-Este programa vai fazendo uma divisão passo a passo, mostrando o
resto e o quociente a medida do caminho

b.-Temos dois operador para a divisão, a divisão normal, que
retorna um número float, isto é, um número com casas decimais e o do
módulo que mostra o resto da divisão

c.
"""
dividendo = int(input("Dividendo: "))
divisor = int(input("Divisor: "))
if dividendo<divisor:
    print("O dividendo tem de ser maior do que o divisor.")
else:
    resto = dividendo
    quociente = 0
    while resto >= divisor:
        resto = resto - divisor
        quociente = quociente + 1 
        print("O quociente é", quociente, "e o resto é", resto)
#d.
dividendo = int(input("Dividendo: "))
divisor = int(input("Divisor: "))
if dividendo<=0 or divisor<=0:
    print("O dividendo e o divisor têm de ser números positivos.")
elif dividendo<divisor:
    print("O dividendo tem de ser maior do que o divisor.")
else:
    resto = dividendo
    quociente = 0
    while resto >= divisor:
        resto = resto - divisor
        quociente = quociente + 1 
        print("O quociente é", quociente, "e o resto é", resto)

#Exercicio 12
n = int(input("Escreve um número inteiro: "))
current_n = 1
while current_n <= n and current_n<1000:
    i = current_n
    f = 1
    while i > 1:
        f = f * i
        i = i - 1
    print("Factorial de " + str(current_n) + ": " + str(f))
    current_n = current_n + 1

#Exercicio 13
choice="sim"
while choice == "sim" or choice == "Sim":
    n=float(input("Insira um número decimal: "))
    n=n//1
    print("A parte inteira do número é",int(n))
    choice=input("Ainda quer continuar? Insira 'sim' se sim.")

#Exercicio 14
k=int(input("Insira um número inteiro: "))
w=input("Insira uma palavra: ")
for i in range(1,k+1):
    print(i,(w+" ")*i)

#Exercicio 15
k=int(input("Insira um número inteiro: "))
mult=1
for i in range(1,k+1):
    mult=mult*(3**i)
print(mult)

#Exercicio 16
try:
    num=int(input("Insira um número inteiro maior do que 2: "))
    if num<2:
        print("O número tem de ser maior do que 2.")
    else: 
        num_perf=[]
        n=2
        for i in range(2,num):
            if n>num:
                num_perf.pop()
                break
            else:
                n=(2**(i-1))*((2**i)-1)
                num_perf.append(n)
        print("Os números perfeitos entre 2 e",num,"são",num_perf)
except:
    print("Tem de ser um número.")

#Exercicio 17
try:
    num_c=0
    k=int(input("Insira um número inteiro maior do que 10:" ))
    if k<10:
        print("Insira um número válido.")
    else:
        for c in range(10,k+1):
            c=str(c)
            if c==c[::-1]:
                num_c+=1
            else:
                continue
    print(num_c)
except:
    print("Insira um número válido.")
















