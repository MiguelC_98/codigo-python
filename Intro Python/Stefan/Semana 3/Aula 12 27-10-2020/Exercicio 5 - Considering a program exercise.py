#Miguel Correia

"""
Consider this program:
    if x==1:
        x=x+1
        if x==1:
            x=x-1
        else:
            x=x-1
    else:
        x=x-1
If in the beginning of the function the x=1 what is the value of x in the
end of the function

At the end of the function x=1, because it adds 1, x=2, and then it goes
to the else and subtracts 1, x=1


What would have to be the value of x if in the end of the function it had
to be x=-1

x=0

What part of this program would never be executed?

The second if, because if x==1, x+=1, so it can't be x==1 after, ever.
"""