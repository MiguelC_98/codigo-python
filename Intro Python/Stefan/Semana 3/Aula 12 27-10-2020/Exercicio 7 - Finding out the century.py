#Miguel Correia

"""
Write in Python a program that reads a year(>0) and writes which century
it belongs to 
"""
user_year=int(input("Insert the year: "))
if user_year%100==0:
    cent=user_year//100
else:
    cent=(user_year//100)+1
print("The century of that year is:",cent)