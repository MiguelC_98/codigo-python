#Miguel Correia
"""
Input:(4,6)
Output:(4,6,9)

What is my input?
How to convert Tuple to list?
How should i use add or append or extend?
How do i turn a list in a tuple?
"""


t=(4,6)
l=[]
loop=0
while len(l) != len(t):
    l.append(t[loop])
    loop=loop+1
l.append(9)
t=tuple(l)
print(t)

#Easier way...
t=(4,6)
l=list(t)
l.append(9)
t=tuple(l)
print(t)