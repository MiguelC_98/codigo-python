#Miguel Correia

"""
How many tuples are in "num" list
input:
    num=[8,2,(9,3),4,(1,6,7),34]
output:
    t=2
"""
num=[8,2,(9,3),4,(1,6,7),34]
t=0
no_t=0
for x in num:
    if type(x)==tuple:
        t=t+1
    else:
        no_t=no_t+1
print("There are",t,"tuples in the list.")
print("There are",no_t,"items that are not tuples in the list")

