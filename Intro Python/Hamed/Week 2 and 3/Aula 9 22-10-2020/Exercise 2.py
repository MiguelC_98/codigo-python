#Miguel Correia
"""
Input: t="$ $ Python$#Course  $"
Output: t="Python Course"

1.Remove "$", "#" and spaces in t
2-Add space between "Python" and "Course"
"""
t="$ $ Python$#Course  $"
t1=""
sp1=0
sp2=0
t2=""
index=-1
for x in t:    #Makes t1="PythonCourse"
    if x.isalnum()==True:
        t1=t1+x
    else:
        continue
for y in t1:   #Loop to add space in "PythonCourse"
    index=index+1
    if y == "n":
        sp1=index
        t2=t2+y
    elif y=="C":
        sp2=index
        if sp1 == sp2-1:
            t2=t2+" "+y
        else:
            t2=t2+y
    else:
        t2=t2+y
t=t2
print(t)

#-------------------DONE BY TEACHER--------------  
t="$ $ Python$#Course  $"
#t=t.strip("# $")
a=list(t)
b=a.copy()
for i in a: #We make a loop because remove would only remove the first occurence
    if i=="#" or i=="$" or i==" ": #i in ["#","$"," ","\n","\t"]
        b.remove(i)
b.insert(b.index("C")," ")
t="".join(b)
print(t)