#Exercise 1

"""
Input: [(1,2,3),(4,5,6)]
Output: [(1,2,9),(4,5,9)]
Do this without using the one in class
"""

a=[(1,2,3),(4,5,6)]
b=[]
for x in a:
    x=list(x)
    x.remove(x[2])
    x.append(9)
    x=tuple(x)
    b.append(x)
print(b)

#Exercise 2

"""
by using unpacking method define my_obj to avoid raising error?
my_obj=...
for i,j in my_obj:
    print("i is: ",i)
    print("j is: ",j)
    
when can we use "for i,j in my_obj"?
For example by defining my_obj=(1,3)
We get "cannot unpack non-iterable int object"
"""
my_obj=(2,3)
index=0
for i in my_obj:
    if index%2==0:
        print("i is:",i)
    else:
        print("j is:",i)
    index+=1
    
#Exercise 3

"""
Input: [(1,3),(2,4),("A",8)]
Output: [(1,2,"A"),(3,4,8)]
Do not use zip()
"""
tups=[(1,3),(2,4),("A",8)]
index=0
Ltups=[]
while index<len(tups):
    Ltups.append(len(tups[index]))
    index=index+1
minL=min(Ltups)
zip=[]
for x in range(minL):
    t=()
    for y in range(len(tups)):
        t=t+(tups[y][x],)
    zip.append(t)
print(zip)






















