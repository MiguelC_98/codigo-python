#Miguel COrreia

"""
Try zip with 3 input variables?
A=[1,2,"A"]
B=("Python",161-8,0,5)
C={10,12,14,16,18,20}

Output == print(zip(A,B,C)) 
[(1, 'Python', 10), (2, 153, 12), ('A', 0, 14)]

Write program with the same input and output without zip()
"""

a=[1,2,"A"]
b=("Python",161-8,0,5)
c={10,12,14,16,18,20}
index=0
c=list(c)
zipList=[]
minlength=min((len(a),len(b),len(c)))
while index<minlength:
    t=(a[index],b[index],c[index])
    zipList.append(t)
    index=index+1
print(zipList)