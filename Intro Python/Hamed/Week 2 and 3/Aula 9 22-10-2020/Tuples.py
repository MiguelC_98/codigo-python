#Miguel Correia

"""
Use [] to define a list
Use () to define a tuple
"""

t=("English","History","Mathematics")
print(t)
print(type(t)) #<class "tuple">
print(len(t)) #3


#To define a tuple with single obj we should use "," after it

t1=(3) #t1 is integer
t2=(3,) #t2 is tuple
s1=("a") #s1 is string
s2=("a",) #s2 is a tuple


#Acessing the members of a tuple we use [], the same as a list

t=("English","History","Mathematics")
print(t[0]) #English
print(t[1:3]) #("History","Mathematics)
print(t.index("English")) #0
print("English" in t) #True


#Tuples are immutable, you cannot change them after defining
#them

t=("English","History","Mathematics")
t[0] = "art" #Error because you can't change it, only overwrite
#it completely
for i in t:
    print(f'I like to read {i}')

#Max | Min | Sum | Count
t=(1,9,2)
print(sum(t)) #12
print(max(t)) #9
print(min(t)) #1
print(t.count(9)) #1 

#* , + on tuples
t(1,9,2)
print(t*2) #(1,9,2,1,9,2) Always makes a new tuple
print(t+t+t) #(1,9,2,1,9,2,1,9,2)
print((3,6)+(9,)) #(3,6,9)
print((1,2)+(9,6)) #(1,2,9,6)

#Reversed()
print(tuple(reversed(t))) #(2,9,1)

#Tuples are ordered
t1=(1,2)
t2=(2,1)
print(t1==t2) #False, if it was a set it would be True

#Tuples are not changeable!!
#Here we overwrite t with a new value!
t=(4,6)
t= t+(9,)
print(t) #(4,6,9)

#Removing members from a tuple only works if you convert it
t=(4,7,2,9,8)
x=list(t)
x.remove(2)
t=tuple(x)

#Unpacking
t=(4,8)
a,b=t
print(a) #4
print(b) #4

car=("blue", "auto", 7)
color,_,a=car
print(color) #blue
print(_)     #auto
print(a)     #7

#zip,zip(*)
a=(1,2)
b=(3,4)
c=zip(a,b)
x=list(c)
print(x) #[(1,3),(2,4)]
print(x[0]) #(1,3)
print(type(x[0])) #<class "tuple">

#z can be tuple or list
z=((1,3),(2,4)) #OR [(1,3),(3,4)]
u=zip(*z)
print(list(u)) #[(1,2),(3,4)]
z=dict(z) #DOESN'T WORK
u=zip(*z) #DOESN'T WORK
print(list(u))

a=(1,2,"A")
b=(3,4,8)
c=zip(a,b)
x=list(c)
print(x) #[(1,3),(2,4),("A",8)]
print(list(zip(*x))) #[(1,2,"A"),(3,4,8)]

a=[11,22,33]
b=zip(a,range(2))
print(list(b)) #[(11,0),(22,1)]

#How many tuples are in 'num' list
num=[8,2,(9,3),4,(1,6,7),34]
c=0
for i in num:
    if isinstance(i,tuple): #type(i) == tuple: 
        continue
    c+=1
print(c)        #4
print(len(num)-c) #2

a=[(1,2,3),(4,5,6)]
#[(1,2,9),(4,5,9)] We want this output

b=[i[:-1]+(9,) for i in a] #We go from beginning to next to
#last, not printing the last one and we add 9 to the end.
print(b)






























