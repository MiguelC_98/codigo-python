#Miguel Correia

"""
calculate sum of values in dict
d={"a":4,"b":2,"f":1,"d":1,"c":1}
"""
d={"a":4,"b":2,"f":1,"d":1,"c":1}
som=sum(d.values())
print(som)

#Another solution
d={"a":4,"b":2,"f":1,"d":1,"c":1}
s=0
for i in d:
    s+=d[i]
print(s)
