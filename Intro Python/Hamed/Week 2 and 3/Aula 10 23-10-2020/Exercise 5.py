#Miguel Correia

"""
merge 2 dictionaries together
for same keys,sum values
Input:
    d1 = {"x":3,"y":2,"z":1}
    d2={"w":8,"t":7,"z"":5}
Output:
    d={"x":3,"y":2,"z":6,"w":8,"t":7} (if it's the same numbers, sum them)
"""
d1 = {"x":3,"y":2,"z":1}
d2={"w":8,"t":7,"z":5}

for i,j in d2.items():
    if i in d1:
        d1[i]+=d2[i]
    else:
        d1.update({i:j})
print(d1)

#Make it my way later at home

