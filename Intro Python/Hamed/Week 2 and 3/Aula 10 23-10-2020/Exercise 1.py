#Miguel Correia

"""
make a dict of {<key:value>}
value of each key is the number of key occurrences
Input: "abfabdcaa"
Output: {"a":4,"b":2,"f":1,"d":1,"c":1}
"""
stri="abfabdcaa"
l=[]
d={}
for let in stri:
    let=[let]
    l.extend(let)
for i in l:
    d[i]=d.get(i,0)+1
print(d)

#Solution by teacher
s="abfabdcaa"
d={}
for i in s: #No need to turn it into a list, oops.
    d[i]=d.get(i,0)+1
print(d)