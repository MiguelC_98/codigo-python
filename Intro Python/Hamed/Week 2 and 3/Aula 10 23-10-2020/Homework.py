#Miguel Correia

"""
Create a "person" dictionary with these details:
print(len(person)) #4
print(person["phone"]["home"]) #01-4455
print(person["phone"]["mobile"]) #918-123456
print(person["children"]) #["Olivia","Sophia"]
print(person["children"][0]) #Olivia
print(person.pop("age")) #48
"""
person={
        "phone":{"home":"01-4455","mobile":"918-123456"},
        "children":["Olivia","Sophia"],
        "age":48,
        "name":"Sophia"
        }
print(len(person)) 
print(person["phone"]["home"])
print(person["phone"]["mobile"]) 
print(person["children"]) 
print(person["children"][0]) 
print(person.pop("age")) 