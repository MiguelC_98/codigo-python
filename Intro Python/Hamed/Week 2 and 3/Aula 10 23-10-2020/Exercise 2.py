#Miguel Correia


"""
Make a dict of {<key:value>}
value of each key is the number of key occurrences
Input: "a dictionary is a datastructure."
Output:{"a":2,"dictionary":1,"is:1,"datastructure.":1"}
"""
s="a dictionary is a datastructure."
l=s.split()
d={}
for i in l:
    d[i]=d.get(i,0)+1
print(d)