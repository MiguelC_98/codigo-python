#Miguel Correia

#Use {:} to define a dictionary    Dictionary{key:value}

d={
   "brand":"cherry",
   "model":"arizo5",
   "color":"white"
   }

print(type(d)) #<class 'dict'>
print(len(d)) #3


#Dictionaries are NOT ordered, you can only find a certain value by finding it's key

#add new <key:value> to the dictionary

d={
   "brand":"cherry",
   "model":"arizo5",
   "color":"white"
   }

d["year"]="2010" #This can add a new value or overwrite an existing one

print(d["model"]) #arizo5
d["color"] = "Black"
print(d["color"]) #Black

"""
If you want to have a key with several values you can associate a key with a list
set or whatever other data structure you want.

If you have a dictionary with 2 keys which are the same later will overlap the other
"""
# d={
#    "brand":"cherry",
#    "model":"arizo5",  #"brand":"WoW" at the end.
#    "color":"white",
#    "brand":"WoW"
#    }

#acesses the members in tuple or list we use [], which is the same as dict, you can
#also use get() method to find the value

print(d)
x=d.get("model") #arizo5
y=d["model"]     #arizo5
print(x==y)      #True

x=d.get("cylinder")
print(x)  #None boolean,cylinder key not found

x=d.get("cylinder",-1) #if key not found, return -1
print(x)               #-1

d1={"1":"one","2":"two","3":"three"}
x="1" in d1
y=d1.get("4",False) #it can return booleans if you wish.
print(x)
print(y)

"""
You cannot use the get() method or the [] to find specific values, only the keys.
It is also best to use the get() method because in case it doesn't find a certain
key in the dictionary it returns something that isn't an error
"""

#Keys, Values, Items, accessing them one by one, or all of the same time.

d={
   "brand":"cherry",
   "model":"arizo5",
   "color":"Black",
   "year":"2010"
   }

print(list(d.keys())) #["brand", "model", "color", "year"]
print(list(d.values())) #["cherry","arizo5","Black","2010"]
print(list(d.items())) #[(<key1>,<value1>),...] basicaly, [("brand","cherry"),...]

for k,v in d.items(): #<---- UNPACKING
    print(k,":",v) #prints "brand : cherry \n model:arizo5 \n ..."
"""
Because dictionaries only have 2 values, you only need 2 variables to unpack it,
unlike other tuples because if you have more than two value in the tuple you need
more the exact same number of values to print them properly
"""

#Small exercise in case you want to count the keys and values
d1={"1":"one","2":"two","3":"three"}

c=1 #counter
for k,v in d1.items():
    print("key",c,"is:",k)
    print("value",c,"is:",v)
    c=c+1  #c+=1

"""
key 1 is: 1
value 1 is: one
key 2 is: 2
value 2 is: two
key 3 is: 3
value 3 is: 3
"""
#d.pop() gives an error! Expects at least 1 argument here! Because dict are not
#ordered, they don't have indexes, only keys.

d={
   "brand":"cherry",
   "model":"arizo5",
   "color":"Black",
   "year":"2010"
   }

d.pop("model") #removes the key and value it contains
print(d)

#popitem() removes the last item in dictionary, returns the item in output

f=d.popitem()
print(f)
print(type(f)) #<class> "tuple", the key and value returned as a tuple
print(d)

#You can use clear() and del in dictionary!
d.clear() #makes d an empty dictionary
print(type(d))
print(d)
del d #The variable of d is completely removed


#EXAMPLE*******EXAMPLE**********EXAMPLE******
#Solution 1
"""
make a dict of {<key:value>}
value of each key is the number of occurrences
Input: ["x","y","x","y","z","y","x"]
Output: {"x":3,"y":2,"z":1}
"""
a=["x","y","x","z","y","x"]
d={}
for i in a:
    if i not in d:
        d[i]=1
    else:
        d[i]+=1
print(d)

#Solution 2

a=["x","y","x","z","y","x"]
d={}
for i in a:
    d[i]=d.get(i,0)+1 #The first round it makes it d[x]=0+1
print(d)

#Solution 3

a=["x","y","x","z","y","x"]
d={}
for i in a:
    d[i]=d.setdefault(i,0)+1 #if the key is not inside the dict makes the key:0
print(d)

#Setdefault example
d1={}
for i in range(1,101):
    d1.setdefault(i,str(i))
print(d1)
"""
The loops works as if it's asking everything time "is 1 in the dictionary? if not
make it :"1", is 2 there? if not make it :"2"...""
"""

#Copy()
a={}
b=a        #a and b are dependent
c=a.copy() #a and c are independent

#Sort operator module in dict

d={"a":4,"b":2,"f":1,"d":1,"c":1}

import operator
k=operator.itemgetter(1) #sorts by values
print(sorted(d.items(),key=k)) #[('f', 1), ('d', 1), ('c', 1), ('b', 2), ('a', 4)]

k= operator.itemgetter(0) #sorts by keys
print(sorted(d.items(),key=k)) #[('a', 4), ('b', 2), ('c', 1), ('d', 1), ('f', 1)]


#Sorted() sort values
num={"ali":[12,13,8],"sara":[15,6,14],"taha":[5,18,13]}
d= {k:sorted(v) for k,v in num.items()}
print(d)

#update() merge 2 dicts
d1={"x":3,"y":2,"z":1}
d2={"w":8,"t":7,"z":5}
d1.update(d2)
print(d1)
#{'x': 3, 'y': 2, 'z': 5, 'w': 8, 't': 7} "z" got replaced by 5

#Another way to update, this way is better in case you want to keep your old dicts
d1={"x":3,"y":2,"z":1}
d2={"w":8,"t":7,"z":5}
d={}
for i in (d1,d2):
    print(i)
    d.update(i)
print(d)

#And another way to update
d={**d1,**d2}
print(d)


#Zip in dictionaries
k=["red","green"]
v=["#FF0000","#008000"]
z=zip(k,v)
d=dict(z)
print(d)


#Nested dictionaries
family={
        "child1":{"name":"James","age":8},
        "child2":{"name":"Emma","age":20}
        }
print(family)

d1={"child1":{"name":"James","age":8}}
d2={"child2":{"name":"Emma","age":20}}
family={
        "child1":d1,
        "child2":d2
        }


#randomly choose between dicts
d={
   "F":0,
   "B":0
   }
import random
for _ in range(17):
    d[random.choice(list(d.keys()))]+=1
print(d)
#This code randomly chooses between a dictionary to add 1 to the
#value of the randomly chosen key.


#Python 3.9 changes!!!
"""
Merge(|) and update(|=) operators have been added to the built-in
dict class, they complement dict.update and (**d1,**d2)
"""













