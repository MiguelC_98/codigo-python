#Miguel Correia

"""
Make a dict of {<key:value>}
value of each key is the number of key occurrences
Input: "a dictionary is a datastructure"
        "a set is also a datastructure."
Output:{'a': 4, 'dictionary': 1, 'is': 2, 'datastructure': 2, 'set': 1, 'also': 1}
"""
s1="a dictionary is a datastructure\na set is also a datastructure."
d={}
l=s1.split()
L_word=""
for i in l:
    if i == l[(len(l)-1)]:
        for x in i:
            if x == ".":
                pass
            else:
                L_word=L_word+x
    else:
        d[i]=d.get(i,0)+1
    if len(L_word)>0:
        d[L_word]=d.get(L_word,0)+1
    else:
        pass
print(d)

#Or if not only the last word has a dot...

s1="a dictionary is a datastructure\na set is also a datastructure."
d={}
l=s1.split()
for i in l:
    L_word=""
    for x in i:
        if x == ".":
            break
        else:
            L_word=L_word+x
    d[L_word]=d.get(L_word,0)+1
print(d)

#Done by the teacher***********************Done by the teacher
lines="a dictionary is a datastructure\na set is also a datastructure."
"""
# line1=lines.split("\n")[0]
# line1=line1.split(".")[0]

line1=lines.split("\n")[0].split(".")[0]
# line2=lines.split("\n")[1]
# line2=line2.split(".")[0]
# line2=line2.replace(".","") this works too
line2=lines.split("\n")[1].split(".")[0]
"""
d={}
number_of_lines=len(lines.split("\n"))
for i in range(number_of_lines):
    line=lines.split("\n")[i].split(".")[0]
    s=line.split()
    for i in s:
        d[i]=d.get(i,0)+1
print(d)














