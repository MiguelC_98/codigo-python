#Miguel Correia

#Define sets
f={"apple","orange","banana"}
D=set() #Empty set

#f[0] Error:"set" object not subscriptable
print(type(f)) #<class "set">
print(len(f)) #3
print(f) #("orange","banana","apple")
for i in f:
    print(i) #banana, apple, orange

m=set(("orange","banana","apple"))
print(f==m) #True
print("cherry" in f) #False 

#add() | update() | remove()
f={"apple","orange","banana"}

f.add("cherry") #Can only take one argument
print(f) #{'banana', 'apple', 'cherry', 'orange'}

f.update(["mango","grapes"]) #needs double bracket or.. it goes crazy
print(f) #{'apple', 'banana', 'grapes', 'cherry', 'mango', 'orange'}

f.remove("apple")
print(f) #{'banana', 'grapes', 'cherry', 'mango', 'orange'}

f={"a","b","c"}
f.add("d")
f.add("e","f") #cannot add more than one element
f.add(("e2","f2")) #can add a tupple
f.add(["e2","f2"]) #cannot add list

f={"a","b","c"}
f.update("g") #it works, but it adds character by character
f.update("h","i")
f.update(["h","i"])
f.update("hello") #as you can see here, it goes character by char

#Remove() | Discard() | Copy() | pop() | clear()
vowels={"a","e","i","o","u"}
# "k" is not in vowels
vowels.remove("k") #Error, "k" isn't in vowels

vowels.discard("k") #removes if k is there, and if it isn't it
#doesn't show an error

v2=vowels #v2 and vowels are dependent
c=vowels.copy() #c and vowels are independent

print(vowels)
x=vowels.pop() #removes randomly one of the members because sets
#are not indexed.
print(x) #a
print(vowels) #("u","o","i","e")
print(v2) #("u","o","i","e")
print(c) #("u","o","i","e","a")

vowels.clear()
print(vowels) #set()
print(len(vowels)) #0
del c #completely deletes c

"""
Math stuff review!

A or B == Union | A and B == Interssection
B-A == B-Intessection with A, so B-A == B-(A and B)
Symmetric difference == (A or B) - (A and B)
"""
#Union, Interssection & update
x={1,2,3}
y={2,3,4}
print(x.union(y)) #{1,2,3,4}, all of the members inside x and y
print(x | y) #| is or, {1,2,3,4}, removes all duplicates

x={1,2,3}
y={2,3,4}
print(x.intersection(y)) #{2,3}
print(x & y) #{2,3}, & is and, only prints the duplicates

x={1,2,3}
y={2,3,4}
x.update(y) #same as union, but overwrites the variable
print(x) #{1,2,3,4}


#Difference | Difference_update | Symmetric_difference | ^
A={1,2,3,4,5}
B={2,4,7}

print(A-B) #{1,3,5} removes all the elements that are in B
print(B-A) #{7}

r=A.difference(B) #A-B
print(r) #{1,3,5}
print(A) #{1,2,3,4,5}
print(B) #{2,4,7}

x={1,2,3}
y={2,3,4}
print(x.symmetric_difference(y)) #{1,4} reverse of (x|y)-(x&y)
print(x^y) #{1,4}, ^ is XoR
print(x.union(y)-x.intersection(y)) #{1,4}, same as sym dif
print(x.union(y)-y.intersection(x)) #{1,4}, the intersections are
#always the same, as well as the union

A={1,2,3,4,5}
B={2,4,7}
r=A.difference_update(B) #changed A to the difference of A & B
print(r) #None
print(A) #{1,3,5}
print(B) #{2,4,7}

"""
All of the _update functions overwrite to the value, therefore
you don't need more variables for it
"""

#Update
s="Hamed" #string, it splits it character by character to update
a=[13,25] #list
t=(7,8) #tuple
d={"one":1,"two":2} #Only the keys are considereded when updating
x={56,98} #set
x.update(s,a,t,d)
print(x) #("one",98,"two",7,8,13,"a","H",56,25,"d","m","e")


#isdisjoint
x={1,2}
y={1,2,3}
print(x.isdisjoint(y)) #False, returns True if x and y don't have
#intersection

x={1,2}
y={3,7,8}
print(x.isdisjoint(y)) #True


#issubset
"""
We say that A is a subset of B if every member of A is in B
"""
A={1,2,4}
B={1,2,3,4,5}
print(A.issubset(B)) #True
print(B.issubset(A)) #False, because not all elements of B are in A

C=set()
print(C.issubset(A)) #True, all empty sets are subsets of every set
print(A.issubset(C)) #False, all empty sets have no subsets

"""
a=2
b=4
[a,b]=all numbers between 2 and 4, including 2 and 4 or a<=b
(a,b]=all numbers between 2 and 4, except 2 or a<X<=b
[a,b)=all numbers between 2 and 4, except 4 a<=X<b
(a,b)=all numbers between 2 and 4, except 2 and 4 a<X<b
"""






















