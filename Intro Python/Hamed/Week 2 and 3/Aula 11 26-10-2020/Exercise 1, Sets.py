#Miguel Correia

A={1,2,3,4,6,9,10}
B={1,3,4,9,13,14,15}
C={1,2,3,6,9,11,12,14,15}

#Part 1
D=C-(A&B&C) #C-A.intersection(B).intersection(C), or only A&B
D=C-A.intersection(B,C) #This also works
D=C.difference(A&B&C) #And this ALSO works
print(D)

#Part 2
E=(A&B)-C
E=A&B-C
E=A.intersection(B)-C
E=A.intersection(B).difference(C)
print(E)

#Part 3
F=(A|B)-C #This one requires parenthesis to get the right answer
F=A.union(B).difference(C)
F=A.union(B)-C
print(F)