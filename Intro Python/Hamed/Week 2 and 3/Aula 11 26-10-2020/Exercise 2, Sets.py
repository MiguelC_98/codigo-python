#Miguel Correia

"""
Which characters of "a","y","c","o","z" are in "Python Course"?
Output:
    "o","y"
"""
phrase=set("Python Course")
characters={"a","y","c","o","z"}
print(phrase.intersection(characters))
print(phrase&characters) #This also works