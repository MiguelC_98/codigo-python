#Miguel Correia

d={"x":3,"y":2,"z":1,"y":4,"z":2}
#The dictionary can't accept the same keys with different values
r={}
for k,v in d.items():
    if k not in r.keys():
        r[k]=v
print(r) #{"x":3,"y":2,"z":2}
#So from the start there are no duplications all you have to do is
d={"x":3,"y":2,"z":1,"y":4,"z":2}
print(d)