#Miguel Correia

"""
Count the number of "s":True in students
"""

students=[
          {"id":123,"name":"Sophia","s":True},
          {"id":376,"name":"William","s":False},
          {"id":934,"name":"Sara","s":True}]
num_s=0
for x in students:
    for y in x.items():
        if True in y and "s" in y:
            num_s+=1
print(num_s)

#Done by adding boolean values
num_s=0
for x in students:
    for y in x:
        if y == "s":
            num_s=num_s+x[y]
print(num_s)

#Done by teacher**********Done by teacher************
sum_of_True=0
for i in students:
    sum_of_True+=i["s"]
print(sum_of_True)

#One line code 
print(sum(d["s"] for d in students))