#Miguel Correia

"""
Find match key:value in 2 dictionaries
d1={"a":1,"b":3,"c":2}
d2={"a":2,"b":3,"c":1}
Output:
    {"b":3}
"""
#Without sets
d1={"a":1,"b":3,"c":2}
d2={"a":2,"b":3,"c":1}
d3={}
for k,v in d1.items():
    for x,y in d2.items():
        if x == k and y == v:
            d3={k:d1[k]}
print(d3)

#With sets
d1={"a":1,"b":3,"c":2}
d2={"a":2,"b":3,"c":1}
d1_2=set(d1.items())
d2_2=set(d2.items())
inter=d1_2&d2_2
for i in inter:
    dic_of_inter={i[0]:i[1]}
print(dic_of_inter)