#Miguel Correia

"""
Input:
    s="Python Course"
    x=["o","r"]
Output:
    {"o":2,"r":1}
"""
s="Python Course"
x=["o","r"]
values=[s.count("o"),s.count("r")]
d=dict(zip(x,values))
print(d)

#Or from using code from exercise 1 from last class...
d={}
for i in s: 
    d[i]=d.get(i,0)+1
d_new={}
for i in d:
    if i == "o" or i == "r":
        d_new[i]=d[i]
    else:
        continue
print(d_new)

#Or still another way
d={}
for i in s: 
    if i in x:
        d[i]=d.get(i,0)+1
print(d)

#Or with setdefault
d={}
for i in s: 
    if i in x:
        d[i]=d.setdefault(i,0)+1
        d[i]+=1
print(d)