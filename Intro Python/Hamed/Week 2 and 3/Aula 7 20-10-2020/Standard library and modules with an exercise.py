#@Author: Miguel Correia

#The functions in python are called "Built in functions", no 
#need to install anything to use the functions

#See class 7 powerpoint

#Some modules are not written in python (for example some are in
#C), some of the modules are like math,sys,re,time,etc.

import math
print(math.pi)
import math as m 
print(m.pi)
print(math.pi) #Only works if "import math" is on the code

#Some modules have smaller modules, called submodules

from os import getcwd
print(getcwd())
from os import getcwd as a
print(a())
b=a()
print(b)

#You can import more than one submodules at a time

from math import e,pi
print(e)
print(pi)

#Exercise!!!!!!***********EXERCISE**********

from numpy import array as a
from matplotlib import pyplot as plt
data = a([-20,-3,-2,-1,0,1,2,3,4])
plt.boxplot(data)

#BAD WAY OF IMPORTING*************************************

from math import * #It's a bad way because it uses a lot of
                   #variable names
pi

#**********************************************************

# sum??
# from numpy import * ##Use on console
# sum??

# Because of the way that from x import * works it will overwrite
#existing, if they have the same function name, functions, not 
#making it able to use both.

dir() #Is a function that shows what functions or submodules
#a module has.
#If i have a string or intiger it shows what functions i can
#use with it, for example if i can use "a".isupper()

import math
a="a"
dir(a)
dir(math)

def myfunc():
    """
    Does 1+1

    Returns
    -------
    x : 2

    """
    x=1+1
    return x

myfunc().__doc__
help(myfunc())

#More module examples
import sys
print(sys.getwindowsversion())


import datetime
now=datetime.datetime.now()
print(now)   #2020-10-20 hour:minutes:seconds.microseconds
print(now.year) #2020
print(now.month) #10
print(now.day) #20
print(now.microsecond) #??? whatever microseconds we're on

#If you try to ?? when you have () on a function you need
#to remove them if you want to check 

#If the name of the module and submodule are the same you
#cannot use TAB to check what functions you can use













