import datetime
minute=datetime.datetime.now().minute
mt=minute%2
if mt == 0:
    print("The minute is even and it is,",minute)
else:
    print("The minute is odd and it is,",minute)
    
#Without the .minute

import datetime
now=datetime.datetime.now().utctimetuple()
minute=now[4]
evodd=minute%2
if evodd==0:
    print("The minute is even and it is,",minute)
else:
    print("The minute is odd and it is,",minute)
    
#with lists

from datetime import datetime as dat
m=dat.now().minute
even=[0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,
      40,42,44,46,48,50,52,54,56,58,60]
# odd=[1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,
#      41,43,45,47,49,51,53,55,57,59]
if m in even:
    print("The minute is even and it is,",m)
else:
    print("The minute is odd and it is,",m)
    
#with for

from datetime import datetime as dat
m=dat.now().minute
for x in range(0,61,2):
    if x == m:
        print("The minute is even and it is,",m)
        break
    else:
        continue
for x in range(1,60,2):
    if x == m:
        print("The minute is odd and it is,",m)
        break
    else:
        continue