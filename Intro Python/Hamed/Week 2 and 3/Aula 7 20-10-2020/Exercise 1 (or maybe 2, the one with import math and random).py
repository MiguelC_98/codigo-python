#@Author: Miguel Correia

#Exercise 1

#MATH IMPORT

import math
print(math.fmod(9,4))  #9%4
print(math.gcd(30,4))  #the greatest divider between 30,4 (2)
print(math.fabs(-4))   #absolute value (turns to positive) (4)

#RANDOM IMPORT

import random as r
print(r.randint(1,5))  #random intiger between 1-5
print(r.choice([1,5])) #random choice between 1 and 5
a=[1, 2, 3, 4]
r.shuffle(a)           #shuffles how a list is organized
print(a)

#For more info:
#Math : https://docs.python.org/3.8/library/math.html
#Random : https://docs.python.org/3.8/library/random.html
