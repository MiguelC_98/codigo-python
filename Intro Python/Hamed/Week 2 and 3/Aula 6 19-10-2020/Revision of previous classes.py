#Miguel Correia

#String methods
name="Josi"
age=31
print("%s is %d years old"%(name,age))
print("{} is {} years old".format(name,age))
print(f"{name} is {age} years old")
#Simplified
print(name,"is",age,"years old")
print(name,"is",str(age),"years old")


#Names for variables
print("a2".isidentifier()) #Returns True because "a2" can be used as
                           #a variable, unlike "2a" that returns
print("2a".isidentifier()) #false, because it starts with a number

#You also cannot use reserved words as variable names, like import
#False, return, etc...
from keyword import iskeyword
print(iskeyword("if")) #checks to see if the word "if" is a keyword

#Variable names examples
print("a2".isidentifier())     #True
print("2a".isidentifier())     #False
print("_myvar".isidentifier()) #True
print("my_var".isidentifier()) #True
print("my-var".isidentifier()) #False
print("my var".isidentifier()) #False
print("my$".isidentifier())    #False
print("my#".isidentifier())    #False


#Multi assignement

a=5
b=1
print("Five plus one is equal to {a+b}") #Five plus one is equal
#to {a+b}
print(f"Five plus one is equal to {a+b}") #Five plus one is equal
#to 6

a=b=c=5 #Assigns the value of 5 to the variables a,b and c
print(a,b,c) #5 5 5,a,b and c are independent

x=1
y=2
y,x=x,y  #swaps the values of x and y
print(x) #2
print(y) #1

#Data types examples
s="Python Course"
print(type(s)) #str

i=2
print(type(i)) #int

f=2.5
print(type(f)) #float

c=2+3j         #2 is the real part and 3 is the imaginary
print(type(c)) #complex

print(bool(5))       #True
print(bool(-2))      #True
print(bool("Hamed")) #True

print(bool(0))       #False
print(bool(""))      #False

print(bool([]))      #False
print(bool({}))      #False
print(bool(()))      #False

b=True
c=5>2
print(type(b))       #bool
print(type(c))       #bool

l=["apples","oranges","grapes"]
print(type(l))  #list

t=("apples","oranges","grapes")
print(type(t))  #tuple

d={"id":"123","name":"farshid"}
print(type(d))  #dict

s={"apples","oranges","grapes"}
print(type(s))  #set


#Receiving input from Console
a=int(input("Insert a: "))
b=int(input("Insert b: "))
c=a+b
print(c)

#Output

n=12.5
print("%i"%n) #12
print("%f"%n) #12.500000
print("%e"%n) #1.250000e+01


#Arrithmetic operators
print(1+3)  #4 addition
print(5-3)  #2 subtraction
print(2*3)  #6 multiplication
print(3/2)  #1.5 unrounded division (returns a float)
print(3//2) #1 rounded to minus infinite division (returns an int)
print(17%5) #2 remain of a division
print(2**3) #8 exponentiation
print(0**0) #0 exponentiation with only 0's
print(6**0) #0 exponentiation with 0

#Operators Precedence examples
print(8-2*3) #2
print(1+3*4/2) #7.0
print(16/2**3) #2.0
print(2**2**3) #256, 2**3 comes first

#Augmented assignement operators
x=4
x+=2 #x=x+2
print(x) #6

y=8
y//=2 #y=y//2
print(y) #4

#Comparission operators
print(2==3) #False
print(2!=3) #True
print(2<3)  #True

#Logical operators
print(1<3 and 4>5) #False
print(1<3 or 4>5)  #True
print(not 1<3)     #False

#Short-circuit operators
print(1>=2 and (5/0)>2) #False, because it only needed read the 
#first argument to find out it was False
print(3>=2 and (5/0)>2) #Error, because it read (5/0) which is
#impossible

#Membership operators
x=[1,2,3,4,5]
print(3 in x) #True
print(24 not in x) #True

#Bitwise operators
a=13
print(bin(a)) #1101
b=14
print(bin(b)) #1110

c=a|b
print(bin(c)) #1111
c=a&b
print(bin(c)) #1100
c=a^b
print(bin(c)) #0011

a=13
print(a<<1) #26, shifts one bit to the left
a=20
print(a>>1) #10, shifts one bit to the right
a=18
print(a>>2) #4, shifts two bits to the right

#Operations on strings
s1="Python"
s2="Course"
s3=s1+s2
print(s3) #Python Course

s="sara"
print(s*2) #sarasara


#Conditional statements and expression
import math
n=-16
if n<0:
    n=abs(n)
print(math.sqrt(n)) #4

#Control statements
a=5
if True:
    a=6
print(a) #6

#if else example
a=20
if a%2==0:
    print("Even") #Prints this one
else:
    print("Odd")

x=3
y=2
if x==1 or y==1:
    print("ok")
else:
    print("no") #Prints this one

names=["sara","taha","farshid"]
if "ali" in names:
    print("Found")
else:
    print("Not found") #Prints this one

#Finding the minimum between a,b
a=2
b=5
if a<b:
    m=a
else:
    m=b
print(m) #prints a

#conditional statement
m=a if a<b else b #this also works but it's way worse for others
#to read

#Other exampel
my_list=["a","e","i","o","u"]
if "o" in my_list:
    s="Yes"
else:
    s="No"

#conditional statement
s="yes" if ("o" in my_list) else s="no"
print(s) #Yes

x=2
y=6
z=1 + (x if x>y else y+2) #z=1+(y+2)
print(z) #9

grade=12
s="fail" if grade<10 else "pass"
print(s) #"pass"

score=75
if score >=90:
    l="A"
else:
    if score >=80:
        l="B"
    else:
        if score >= 70:
            l="C"
        else:
            l="D"
print(l) #"C"

#Using the elif
score=75
if score >= 90:
    l="A"
elif score >= 80:
    l="B"
elif score >= 70:
    l="C"
else:
    l="D"
print(l) #"C"


#LOOPS********LOOPS*******LOOPS*********LOOPS*********

#using the range() function
for j in range(5,10,2):
    print(j, end=" ") #5,7,9

s="Python"
for ch in s:
    print(ch) #prints python character by character

for _ in range(13):
    print("Hello") #prints 13 hello's

for i in range(4): #the start is not specified therefore it's 0
    print(i, end = " ") #0 1 2 3 

for i in range(3,8): #starts at 3 instead of 0
    print(i, end = " ") #3 4 5 6 7

#Count the number of characters in a word!
word="Python"
c=0
for i in word:
    c=c+1 #c+=1
print(c) #6

#Step of -3 in range function
for i in range(9,2,-3):
    print(i, end = " ") #9 6 3

#Count how many "a"'s are in a word
word="alireza"
c=0
for i in word:
    if i=="a":
        c+=1
print(c) #2

#Count how many vowels there are
name="farshid"
v="aeiou"
c=0
for i in name:
    if i in v: #a and i
        c+=1
print(c) #2

#Nested for range examples
name="farshid"
v="aeiou"
a=[ch for ch in name if ch in v] 
print(a)  #["a","i"]

for i in range(1,4):
    for j in range(2,4):
        print(j, end = " ")
    print()
"""
i=1 j=2 j=3
i=2 j=2 j=2
i=3 j=2 j=3
"""

#break, continue examples
for i in range(5):
    if i == 3:
        break #ends the loop at 3
    else:
        print(i, end = " ") #0 1 2

for i in range(5):
    if i == 3:
        continue #at 3 resets back to the start
    else:
        print(i, end = " ")#0 1 2 4 

#While loops
i=1
while i <= 3:
    print(i, end = " ") #1 2 3
    i+=1

n=7
while n>=3:
    print(n,end = " ") #7,6,5,4,3
    n-=1

s="abcdef"
i=0
while True:
    if s[i] == "d":
        break
    print(s[i],end=" ")#a b c 
    i+=1

n=8
while n>2:
    n-=1
    if n == 5:
        break
    print(n,end=" ")#7 6

n=8
while n>2:
    n-=1
    if n==5:
        continue
    print(n,end=" ")#7 6 4 3 2


#PEP8************PEP8************PEP8***********PEP8**********
#This is not standard PEP8
n=1
while n<=3: print(n) ; n+=1

#This is standard PEP8
n=1
while n<=3:
    print(n)
    n+=1



#GUESSING GAME
import random
n=random.randomrange(0,10) #picks a random number from 0-9
f="no"
while f == "no":
    a=int(input("Game:Guess number between 0 and 9: "))
    if a<n:
        print("Increase")
    elif a>n:
        print("Decrease")
    else:
        print("Correct! You won!")
        f="Yes"
print("Thank you.")



















