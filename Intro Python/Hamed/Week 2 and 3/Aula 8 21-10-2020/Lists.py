#Miguel Correia

#Lists******Lists********Lists********
a=[5,7,12]
print(a[0])
print(a[1])
print(a[1:2])
print(a[:])
print(type(a))
print(len(a))

b=[1.234,"Python",0, {"joe":21},(3,6,9),[],(3,6,9)]
print(b[3])
print(type(b[3]))

my_list=["p","r","o","b","e"]
print(my_list[-1])
print(my_list[0]) #== my_list[-5]

a=[5,7,12]
print(a.index(7))
a[1]=8
print(a)

#Indexing in strings
s = "sara"
print(s[1]) #prints "a"
print(s.index("a")) #prints 1, the first occurence of "a"
s[1]="d" #This gives an error, explanation below.

#Strings are immutable so you can't do like lists and change
#a value of an index.

a=[1,2]
b=[2,1]
print(a==b) #False, because lists are ordered, therefore they
#are not the same.

#Other data type that are ordered are: Tuples, strings and dict
#but only on newer versions of python.

#Data types that are not ordered: Sets and dict but on older
#python versions.

#How to show the items in a list one by one
friends = ["Hamed","Josi","Stefan"]
for f in friends:
    print(f)
#Or
for i in range(len(friends)): #len(friends) = 3
    print(friends[i])



#slicing in a list

# list_name[Start:Stop:Step]
names=["Miguel","Josi","Hamed","João"]
print(names[0:3:2]) #Because it doesn't count the last index
#it has to go to 3 to print "Hamed"
a=[7,5,30,2,6,25]
print(a[1:4])
print(a[:3]) #Prints until index 3, not counting it
print(a[3:]) #Prints from index 3, counting it because it's
#the start
print(a[3:0]) #Prints nothing because you're starting from
#index 3 to index 0 with a step of +1
print(a[3:0:-1]) #Prints from index 3 to index 0, not
#counting 0, because we give it a step of -1
print(a[::-1]) #Reverses the list because you're saying to
#start from the start, and end at the end, but we don't give
#it values, instead we give it a step of -1, reversing the
#whole list.
print(a[0:7:2]) #prints from start to finish 2 by 2 indexes
print(a[6:0:-2]) #prints from the end to the start, 2 by
#2 indexes
print(a[50:0:-2]) #We can print from index 50 because we're
#giving it a step of -2, it doesn't find anything until 
#index 6 so it doesn't print anything.
print(a[:0:-2]) #Prints from the end to finish 2 by 2
a[3:5]=[14,15]
print(a) #[7,5,30,14,15,25]

#Repeat and Concatenate lists with * and +

a=[4,7]
b=a*2
print(b) #[4,7,4,7]
a=[1,2]
b=["a","b","c"]
c=a+b #You can also you c=a.extend(b)
print(c) #[1,2,"a","b","c"]

#Using in and not in

a=[7,6,30,2,6,25]
print(14 in a)     #False
print(14 not in a) #True

#Lists in a list

a=[3,[109,27],4,25]
print(a[1]) #[109,27]
print(a(1)) #Error Need to use [] not ()
print(a[1][1]) #27
print(a[1,1]) #Error you cannot use a comma
print(len(a)) #4, the lists count only as 1

#counting the number of occurrences of a value
a=[1,3,6,5,3]
print(a.count(3)) #prints 2, because there are 2 3's in the
#list

#inserting objects before index
a=[1,2,6,5,2]
a.insert(2,13) #inserts(index,obj), puts 13 in the index 2
#and passes 6 to index 3, and so on.
print(a) #[1,2,13,6,5,2]

#remove()
a=[1,2,6,5,2]
a.remove(2) #only removes the first occurrence of an object
print(a) #[1,6,5,2]

#pop()
x=[10,15,12,8]
a=x.pop() #removes and returns the item at the index, 
#starting from the end.
print(x) #[10,15,12]
print(a) #8
y=["a","b","c"]
p=y.pop(1) #pops the object at the index
print(p) #b
print(y) #["a","c"]

#del()
a=[5,9,3]
del(a[1]) #This DOESN'T return the object
#b=del(a[1]) #Gives an error because it has no value
print(a) #[5,3]

#slicing with del
a=[0,1,2,3,4,5,6]
del(a[2:4]) #deletes object at the index 2 and 3
print(a) #[0,1,4,5,6]

#Reverse and Sorting
a=[1,2,3]
print(a[::-1]) #[3,2,1]
a.reverse()
print(a) #[3,2,1]

b=a.reverse() #b is None
print(b) #None

a=[2,4,3,5,1]
a.sort()
print(a) #[1,2,3,4,5]

#extend()
x=[1,2,3]
x.extend(5) #Error
x.extend([5]) #It has to be in list form
print(x) #[1,2,3,5]
x=[1,2,3]
y=[4,5]
x.extend(y) #makes x into [1,2,3,4,5]
print(len(x)) #5, because there are 5 objects now
print(len(y)) #2, because the list is still there

#append()
a=[1,2,3]
a.append(4)
print(a) #[1,2,3,4]

x=[1,2,3]
y=[4,5]
x.append(y)
print(x) #[1,2,3,[4,5]]
print(len(x)) #4, because y counts as 1 object in append
print(len(y)) #2

a=[]
for i in range(0,5):
    a.append(i)
print(a)

#clear()
a=[1,2,3]
a.clear()
print(a) #[] the list is still there but empty
print(len(a))

#copy()
a=[1,2,3]
b=a.copy()
print(b) #[1,2,3]

c=a
d=a[:]
"""
The copy function is better because a, b and d are 
independent, you can change a and nothing happens to them
but c is dependent, so if you, for example, del(a), c will
be deleted too.
"""
a[1]=22
c[0]=11
d[2]=33
print(a) #[11,22,3]
print(b) #[1,2,3]
print(c) #[11,22,3]
print(d) #[1,2,33]

x=2
y=x
y+=1
print(x) #2
print(y) #3
#Because these are intigers and not lists, they are not
#dependent on each other


x=[]
y=x
y.append(5)
print(x) #5
print(y) #5
#Here they're lists, so they are saved in the same location
#in the memory

a=[i for i in range(4)] #[0,1,2,3]
print(a)
a=[i*2 for i in range(4)] #[0,2,4,6]
print(a)
a=[i*3 for i in range(3,6)] #[9,16,25]
print(a)

a=[1,-2,5,-56,8]
b=[abs(i) for i in a]
print(b) #[1,2,5,56,8], it changed the value to positive

import math
a = [round(math.pi,i) for i in range (1,5)]
print(a) #[3.1,3.14,3.142,3.1416], it rounds the value up

a=["$ali","sara$"]
b=[i.strip ("$") for i in a]
print(b) #["ali,"sara"] it stips "$" for each string

#How to convert list to string
a="$$$$$$##Jo$11$$$$$"
b=list(a) #['$', '$', '$', '$', '$',...]
print(b)
c="".join(b) #"$$$$$$##Jo$11$$$$$"
print(c)


a=[2.6,float("NaN"),4.8,6.9,float("NaN")] #NaN = Not a num
b=[]
import math
for i in a:
    if not math.isnan(i):
        b.append(i)
print(b) #[2.6,4.8,6.9]
"""
Because we're using the list in the loop as we want to
change it tries to count the next index, if you remove
an item it fails, so we use a "copy" of the list to avoid
it.

If you want to change length of a list, dict or set in a
loop and want to remove/add some object you need to check
the iterable variable in your loop. If you used the same
list/dict or set you need to make a copy
"""































