m = [
     [1,2,3],
     [4,5,6],
     [7,8,9]
     ]
"""
1-print first row
2-print first column 1 4 7 in a single line 
3-print main diameter 1,5,9
4-print another diameter 3 5 7
5-calculate sum of rows (1+2+3), (4+5+6),(7+8+9)
6-calculate sum of columns (1+4+7),(2+5+8),(3+6+9)
"""

#Exercise 1
loop=0
for x in m:
    if loop == 0:
        print(x)
    else:
        break
    loop=loop+1


#Exercise 2
for x in m:
    for n in x:
        if x[0]==n:
            print(n, end = " ")
        else:
            break
print()


#Exercise 3
loop=0
for x in m:
    for y in range(len(m)):
        if y == loop:
            print(x[y], end = " ")
            break
        else:
            continue
    loop=loop+1
print()


#Exercise 4
loop=len(m)-1
for x in m:
    while loop != -1:
        print(x[loop], end = " ")
        break
    loop=loop-1
print()


#Exercise 5

for x in m:
    print(sum(x), end = " ")
print()


#Exercise 6
m = [
     [1,2,3],
     [4,5,6],
     [7,8,9]
     ]

def col(m,i):
    l=[]
    for x in m:
        for n in x:
            if x[i]==n:
                l.append(n)
            else:
                continue
    return l

i=0
som=[]
while i<len(m):
    tsom=sum(col(m,i))
    tsom=[tsom]
    if som==[]:
        som=tsom
    else:
        som.extend(tsom)
    i=i+1
print(som)
        

    




















