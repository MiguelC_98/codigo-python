#Miguel Correia


#Exercise 2*********Exercise 2**********

a=[7,5,30,2,6,25]
a.sort()
max_value=a[len(a)-1]
print(max_value)

print(max(a)) #Finds the max value
print(min(a)) #Finds the min value
print(sum(a)) #Sums all items in a list

#The index of the biggest number
print(a.index(max_value))

#The sum of all numbers
som=0
for x in a:
    som=x+som #som+=x
print(som)

#Other way of finding index of the biggest number without
#changing it
a=[7,5,30,2,6,25]
L_num=a[0]
for x in a:
    if x>L_num:
        L_num=x
    else:
        pass
print(a.index(L_num))
