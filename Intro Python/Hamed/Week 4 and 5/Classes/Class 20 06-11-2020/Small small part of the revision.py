# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 08:47:15 2020

@author: Miguel Correia
"""

import pandas as pd
import numpy as np
b=np.array([2,3,4])
"""
array is like a list, but different, same but different, because when 
you use array you are limited to one datatype, for example you can't have
an int and a float number in an array. Array's are still indexed though.
When you use np.array().dtype you are calling a function inside the 
numpy array class.

In a matrix the rows always come first than the columns, for example
if we have a matrix with five rows and three columns and want a specific
item you do [4:2](5th row, 2nd column). If you want all of the numbers in
a row you call it a horizontal vector [1,:](2nd row, all of the columns) 
and if you want all of the numbers in one column you call it a vertical
vector [:2,] (3rd column, all of the numbers)
"""

