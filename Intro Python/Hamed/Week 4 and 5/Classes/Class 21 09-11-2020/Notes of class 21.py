# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 08:50:07 2020

@author: Miguel Correia
"""
"""
Arrythmetic mean = sum of all members divided for the number of items
example: (x1+x2+x3+x4)/4

Geometric Mean= product of all members elevated by (1/n), n being the 
number of items
example: (x1*x2*x3*x4)**(1/4)

Harmonic mean= number of items divided by the reversed numbers.
example: (4/((1/x1)+(1/x2)+(1/x3)+(1/x4)))


Median: Middle value seperating the greater and lesser halves of a data 
set, aka, the middle number of a sorted data set.
example: dataset=(1,2,2,3,3,4,5), median=3

Mode: Most frequent number in a data set
example: dataset=(1,2,2,2,3,3,4,5,5), mode=2
"""
import statistics
from scipy import stats
import numpy as np

a=[4,36,45,50,75]
b=[1,2,2,3,4,7,9]
c=[6,3,9,6,6,5,9,9,3,1]

print(statistics.mean(a))
print(np.mean(b))
print(np.mean(c))

print(statistics.mode(a))
print(stats.mode(b))
print(stats.mode(c))

print(statistics.median(a))
print(statistics.median(b))
print(np.median(c))

"""
Standart deviation (sd), the measure of the amount of variation or dispersion
of a set of values. A low standart deviation indicates that the value 
tend to be close to the mean of the set, while a high standart deviation
indicates that the values are spread out over a wider range.

(complicated equations check the powerpoint)
(Sigma is the symbol that means the sum of all the items.)

Variance (var), it measures how far a set of numbers is spread out from their
average value. The average of the squared differenced from the Mean.

(actual equation in powerpoint)
sd=sqrt(var) or var=sd**2


Why take a sample?
Mostly because it is easier and cheaper.

Imagine you want to know what the whole country thinks... You can't ask
millions of people, so instead you ask maybe 1k people

To find out information about population (such as mean and standard
deviation), we do not need to plook at all members of the population,
we only need a sample.

BUT when we take a sample, we lose some accuracy.
(Examples are in the powerpoint)
"""

import statistics
import numpy as np

a=[9,2,5,4,12,7,8,11,9,3,7,4,12,5,4,10,9,6,9,4] #population
b=[9,2,5,4,12,7] #sample

print(np.mean(a))
print(np.mean(b))

print(np.std(a))
print(statistics.pstdev(a)) #population
print(statistics.stdev(b)) #sample

print(np.std(b)) #this one is wrong because we're giving it a sample
print(statistics.stdev(b))

"""
(see variance in powerpoint)

Quartile are the values that diviade a list of numbers into quarters
1) Put the list of numbers in order
2) Cut the list in four equal parts
3) The quartilies are the cuts

Sometimes the cut is between two numbers, so the quartile is the 
average of the two numbers.

5 Methods to calculate the Quartiles:
    When the desired quantile lies between two data points i<j
    i=first number
    j=second number
    Linear:
        i+(j-i)*fraction(part of the index surrounded)
    Lower:
        i
    Higher:
        j
    Nearest:
        i or j
    Midpoint:
        (i+j)/2

Interquartile Range:
    The interquartile range is from q1 to q3, aka quartile 1 to
    quartile 3, 50% is between it, and the other is out of it, goes
    from 1/4 before the middle to 1/4 after it.


CHECK POWERPOINT FOR MATPLOTLIB EXAMPLES
"""
import matplotlib.pyplot as plt
x=[0,0.5,2]
y=[0,1,4]
#creates the points A(0,0), B(0.5,1) C(2,4)
plt.plot(x,y,'go--')

x=[1,2,3,4,5]
y=[2,4,6,8,10]
plt.plot(x,y,'r--P',label='y=2x')
plt.legend()
plt.title('test')
plt.xlabel("x")
plt.ylabel("y")

x1=[1,2,3,4,5]
y1=[1,4,9,16,25]
plt.plot(x1,y1,'b:D',label="x^y")
plt.legend()

plt.plot(x,y,"r--P",label="y=x*2")
plt.plot(x1,y1,"b:D",label="x^y")
plt.legend()

x=[1,2,3,4,5]
y=[2,4,6,8,10]
x1=[1,2,3,4,5]
y1=[1,4,9,16,25]
plt.subplot(211)
plt.plot(x,y,'r--P',label='y=2x')
plt.legend()

plt.subplot(212)
plt.plot(x1,y1,"b:D",label="x^y")
plt.legend()

import numpy as np
import matplotlib.pyplot as plt
x=np.arange(14)
y=np.sin(x/2)

plt.step(x,y+2,label='pre(default)')
plt.plot(x,y+2,"o--",color="grey",alpha=0.3)

plt.step(x,y+1,where="mid",label="mid")
plt.plot(x,y+1,"o--",color="grey",alpha=0.3)

plt.step(x,y,where="post",label="post")
plt.plot(x,y,"o--",color="grey",alpha=0.3)

plt.grid(axis="x",color="0.95")
plt.legend(title="Parameter where:")
plt.title("plt.step(where=...)")
plt.show()

"""
See the rest on powerpoint

Difference between subplot as subplots
One is state-based and the other is object oriented, subplots instantly
generate the figures, and you can't really change it, subplot you can
change it.

fig is the whole graphic around the actual plotted function
axis is the thing inside the figure, that contains the actual graphic,
so when you you do something like fig,ax=plt.subplots() you have to
do ax.plot(...) to actually plot it and fig.savefig to save the actual
file.


Boxplot are a standardized way of displaying the distribution of data
based on a five number nummary

minumum >=(Q1-1.5*IQR)
first quartile (Q1)
Median (Q2) (the middle)
third quartile (Q3)
Maximum <= (Q3+1.5*IQR)

1. Tells you the value of your outliers (data that you need to find and remove)
2. Identify if data is symmetrical
3. Determine how tightly data is grouped
4. See if your data is skewed

See the powerpoint for Boxplot on normal distribution
"""
import numpy as np
data=np.array([-10,-5,-2,-1,0,1,2,3,4])
q1=np.quantile(data,.25) #gives quartile 1
q2=np.quantile(data,.50) #gives quartile 2
q3=np.quantile(data,.75) #gives quartile 3
iqr=q3-q1
lv=q1-1.5*iqr
hv=q3+1.5*iqr

"""
bins in histograms divides the distance of data in whatever the value
is, and then shows how much data is shown in between that data.
See the Histogram example in Powerpoint


The numpy.meshgrid function is used to create a rectangular grid out of
two given one-dimensional arrays, representing the cartesian indexing
or Matrix indexing
"""
import numpy as np
x=np.array([1,2,3,4])
y=np.array([7,8])
a,b=np.meshgrid(x,y)
print(a)
print(b)
print(a.shape)

"""
We have a plane (x,y), in this case (2,4), we then want to
automatically generate the Z, basically like we're defining a plane in
math where we have 2x+4y=?(in this case it doesn't matter)
                           
We can see a 3D plot in the Powerpoint

countour - just gives line
countourf- fills the lines with colours
"""
import matplotlib.pyplot as plt
import numpy as np
fig=plt.figure(figsize=(5,5))



















