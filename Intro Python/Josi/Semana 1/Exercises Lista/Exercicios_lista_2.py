#Miguel Correia

#Exercicio 1

while True:
    operation=input("Do you wish to add(+), subtract(-), multiply(*), or divide?(/) ")
    if operation == "add" or operation == "+" or operation == "subtract" or operation == "-" or operation == "multiply" or operation == "*" or operation == "divide" or operation == "/":
        pass
    elif operation=="quit" or operation=="Quit.":
        break
    else:
        print("Please insert a valid phrase or operator.")
        print("If you want to quit, write 'quit'.")
        continue
    while True:
        num1=input("Please insert your first number:")
        num2=input("Please insert your second number:")
        if num1.isdigit()!=True and num2.isdigit()!=True:
            print("Please insert valid numbers.")
            continue
        else:
            num1=float(num1)
            num2=float(num2)
        if operation == "+" or operation == "add":
            value=num1+num2
            print(value)
        elif operation == "-" or operation == "subtract":
            value=num1-num2
            print(value)
        elif operation == "*" or operation == "multiply":
            value=num1*num2
            print(value)
        elif operation == "/" or operation == "divide":
            if num1==0 or num2==0:
                print("Cannot divide by 0. Please insert a valid number.")
            else:
                value=num1/num2
                print("{0:.4f}".format(value))
        else:
            pass
        break
    break
    
#Exercicio 2

print("1 - Pão Alentejano\n2 - Bolo Lêvedo [dos Açores]\n3 - Bolo do Caco [da Ilha da Madeira]\n4 - Broa\n5 – I want to leave")
option=input("Pick an option: ")
while True:
    while option == "1":
        print("Pão Alentejano")
        option=input("Pick another option: ")
    while option == "2":
        print("Bolo Lêvedo [dos Açores]")
        option=input("Pick another option: ")
    while option == "3":
        print("Bolo do Caco [da Ilha da Madeira]")
        option=input("Pick another option: ")
    while option == "4":
        print("Broa")
        option=input("Pick another option: ")
    if option == "5":
        print("Goodbye.")
        break
    elif option <"1":
        print("Pick a valid option.")
        option=input("Pick another option: ")
    else:
        print("Pick a valid option.")
        option=input("Pick another option: ")

#Exercicio 3

x = 5 + ord("A") #Adds 5 to the ascii value of any character
y = 0
while True :
    y = (x % 2) + 10 * y   #Because y=0 if x is even, because it multiplies by 0, it does nothing, but when x is uneven, y gains the value of 1, then it adds 10 and it's exponential because 10*y
    x = x // 2   #Divides x by 2, halving the amount of tries in this code each run
    print ("x =", x, "y =", y) #shows the value of the variables
    if x == 0: #When x=0 it hits zero it breaks this loop and goes to the next one
        break
while y != 0: #because y isn't 0 as soon as the other loops stops this one starts
    x = y % 100 #gives x either the value of 0,1 or 10
    y = y // 10 #starts decresing the number of zeros in y until it reaches 0, when it does the loop stops
    print ("x =", x, "y =", y) #prints the value of the variables
    
#The program will pick up the value of 5 + the value of whatever character and transform it to 1

#Exercicio 4 

n=0
max_n=int(input("What number do you want to reach? "))
while n<max_n:
    n=n+1
    space=" "*n
    print(space + str(n))














































