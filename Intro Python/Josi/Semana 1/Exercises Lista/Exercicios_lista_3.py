#Author: Miguel Correia
#Exercise 1

(5 > 4) and (3 == 5) #False, because one is true and the other false
not (5 > 4) #False because the statement was true and was made false by "not"
(5 > 4) or (3 == 5) #True because one of them is true and that is good enough
not ((5 > 4) or (3 == 5)) #False because the statement was true but turned false by "not"
(True and True) and (True == False) #False because with the "and" if one of them is false by itself is false they become false
(not False) or (not True) #True because or only requires one to be true

#Exercise 2

spam = 10                   #Block 1
if spam == 10:              #Block 1
    print('eggs')           #Block 2
    if spam > 5:            #Block 2
        print('bacon')      #Block 3
    else:                   #Block 2
        print('ham')        #Block 3
    print('spam')           #Block 2
print('spam')               #Block 1

#Exercise 3

#*****************Hard way*********************

def xAdd(x,xR):
    xR=list(xR)
    x=[x]
    xR.extend(x)
    x=x[0]
    xR.sort()
    return xR
num={2, 61, -1, 0, 88, 55, 3, 121, 25, 75}
x25=None
x50=None
x75=None
x100=None
for x in num:
    if x<=25 and x>=0:
        if x25 == None:
            x25=x
            x25=[x25]
        else:
            x25=xAdd(x,x25)
    elif x<=50 and x>=26:
        if x50 == None:
            x50=x
            x50=[x50]
        else:
            x50=xAdd(x,x50)
    elif x<=75 and x>=51:
        if x75 == None:
            x75=x
            x75=[x75]
        else:
            x75=xAdd(x,x75)
    elif x<=100 and x>=76:
        if x100 == None:
            x100=x
            x100=[x100]
        else:
            x100=xAdd(x,x100)
    else:
        pass
if x25 == None:
    print("There are no numbers from 0 to 25")
else:
    print("There are",len(x25),"numbers from 0 to 25")
if x50 == None:
    print("There are no numbers from 26 to 50")
else:
    print("There are",len(x50),"numbers from 26 to 50")
if x75 == None:
    print("There are no numbers from 51 to 75")
else:
    print("There are",len(x75),"numbers from 51 to 75")
if x100 == None:
    print("There are no numbers from 76 to 100")
else:
    print("There are",len(x100),"numbers from 76 to 100")
choice=input("Do you wish to know which are the numbers? ")
if choice == "Yes" or choice == "yes":
    print("From 0 to 25 the numbers are",x25)
    print("From 26 to 50 the numbers are",x50)
    print("From 51 to 75 the numbers are",x75)
    print("From 76 to 100 the numbers are",x100)
else:
    pass
    
#*****************Easy way********************

num={2, 61, -1, 0, 88, 55, 3, 121, 25, 75}
x25=0
x50=0
x75=0
x100=0
for x in num:
    if x<=25 and x>=0:     #can also be "if x in range(0,26):"
        x25=x25+1
    elif x<=50 and x>=26:  #can also be "if x in range(26,51):"
        x50=x50+1
    elif x<=75 and x>=51:  #can also be "if x in range(51,76):"
        x75=x75+1
    elif x<=100 and x>=76: #can also be "if x in range(76,101):"
        x100=x100+1
    else:
        pass
print("There are",x25,"numbers in between 0 and 25")
print("There are",x50,"numbers in between 25 and 50")
print("There are",x75,"numbers in between 50 and 75")
print("There are",x100,"numbers in between 75 and 100")

#I made a hard way because that way you can ask what numbers are the ones in that set

#Exercise 4

txt="Start with The Portuguese: The Land and Its People (3) by Marion Kaplan (Penguin), a one-volume introduction ranging from geography and history to wine and poetry, and Portugal: A Companion History (4) by José H Saraiva (Carcanet Press), a bestselling writer and popular broadcaster in his own country."
print(txt.count("a"))
print(txt[11].isupper())
print(txt[2].islower())
print(txt.index("o"))
print(txt[0:4].isalpha())
print(txt[5].isspace())
print(txt.find("s"))
print(txt.rfind("s"))
print(txt.startswith("A"))
print(txt.isdigit())

#Exercise 5

for num in range(0,17):
    x=str(bin(num))
    print("The number",num,"in binary is",x[2:])

