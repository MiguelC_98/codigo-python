#Miguel Correia

#Exercise 1 a

age_person = 20  #int

#Exercise 1 b

yard_area=200   #int

#Exercise 1 c

user_bankMoney = 1   #float

#Exercise 1 d

favSongs="No tears left to cry", "Fight!"  #string or list


#******************************************************

#Exercise 2

word=input("Write a word to reverse: ")
reverse_word=word[::-1]
print(reverse_word)

#*****************************************************

#Exercise 3

s = "abc"

#Exercise 3 a

print(len(s))

#Exercise 3 b

s=(s[0]*3+s[1]*3+s[2]*3)
print(s)

#*********************************************************

#Exercise 4

s="aaabbbccc"

#Exercise 4 a

print("The first occurrence of b is in",s.index("b"),"and the first occurrence of ccc is in",s.index("ccc"))

#Exercise 4 b

X_s=(s.replace("aaa","XXX"))
aX_s=(X_s.replace("XXX","aXX"))
print(X_s,aX_s)

#***********************************************************

#Exercise 5

x="aaa bbb ccc"

#Exercise 5 1

x_upper=x.replace("aaa bbb ccc","AAA BBB CCC")
print(x_upper)

#Exercise 5 2


x_notAll_upper=x.replace("aaa bbb ccc","AAA bbb CCC")
print(x_notAll_upper)

#Exercise 6

a=10
b=a
c=9
d=c
c=c+1

#a=10| b=10 | c=10 | d=9

#Exercise 7

x=int(input("Give me a number:"))
y=int(input("Give me another number:"))

#Part1

xNew=x
xNew=y
y=x
print(xNew,y)

#Part2

x,y=y,x
print(x,y)

#Exercise 8

print (" Enter a number :")
a = int ( input ())
if a % 2 == 0 and a < 100:
    print ("the number is even and smaller than 100")
else :
    if a >= 100: #This line is wrong if the number is uneven and bigger than 100 because it checks if it's even and smaller than 100, if it isn't it just checks if it's bigger than 100, not caring about if it's even or odd, therefore it always prints if it's bigger than 100, wether it's even or odd
        print ("The number is even and equal or higher than 100")
if a % 2 != 0 and a < 100:
    print ("The number is odd and smaller than 100")
else :
    if a >= 100: #This line is also wrong if the number is even but bigger than 100 for the same reason as the other, not minding if the number is even or odd so always printing if a>100
        print ("The number is odd and equal or higher than 100")

#Therefore the code is wrong because it prints twice, one
#of the statements being untrue and the other true, every 
#time a number is bigger than 100 


