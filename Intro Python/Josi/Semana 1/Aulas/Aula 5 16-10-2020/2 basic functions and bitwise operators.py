#Miguel Correia

#Super basic function
def music_lyrics():
    print("We are here")
    print("We are here for all of us")
music_lyrics()

#Superest basic function with return
def greet():
    return "Hello"
print(greet(),"Rodrigo")
print(greet(),"João")

#Bit operators! This is a long one
a=10
b=3
hifen=30
binary=bin(a&b) #Makes it so when the two have 1, they transform
denary=a&b #into 1 but when only one or none have, it's 0
print("Line 1 - Value of binary is",binary)
print("Line 1 - Value of denary is",denary)
print("-"*hifen)

binary=bin(a|b) #Makes it so only one of the numbers need to have 1
denary=a|b #to make a 1, and two 0's for one 0
print("Line 1 - Value of binary is",binary)
print("Line 1 - Value of denary is",denary)
print("-"*hifen)

binary=bin(a<<2) #adds two 0's on the right side
denary=a<<2
print("Line 1 - Value of binary is",binary)
print("Line 1 - Value of denary is",denary)
print("-"*hifen)

binary=bin(a>>2) #takes off the two first numbers, moving them to
denary=a>>2 #the left
print("Line 1 - Value of binary is",binary)
print("Line 1 - Value of denary is",denary)
print("-"*hifen)

binary=bin(~a) #switches the 0's and 1's, basicaly doing -a-1 (-11)
denary=~a
print("Line 1 - Value of binary is",binary)
print("Line 1 - Value of denary is",denary)
print("-"*hifen)

binary=bin(a^b)
denary=a^b
print("Line 1 - Value of binary is",binary)
print("Line 1 - Value of denary is",denary)
print("-"*hifen)