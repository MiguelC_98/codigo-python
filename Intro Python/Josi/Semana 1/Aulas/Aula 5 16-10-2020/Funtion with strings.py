def food(vegetable):
    if vegetable == "tomato":
        print("I bought a tomato")
    elif vegetable == "cauliflower":
        print("I bought a cauliflower")
    else:
        print("I bought another type of vegetable")
food("tomato")