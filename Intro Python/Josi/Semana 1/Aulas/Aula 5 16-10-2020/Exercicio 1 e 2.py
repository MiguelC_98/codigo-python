#Exercicio 1

num_max=0
while num_max==0:
    num_max=input("Please insert a number: ")
    if num_max.isdigit()!=True:
        print("Please insert a digit.")
        num_max=0
    else:
        num_max=int(num_max)
for num in range(num_max,0,-1):
    if num%2==0:
        print("Even number:",num)
    else:
        print("Odd number:", num)

#Exercicio 2

#Without Math import

num= [2,61,-1,0,88,55,3,121,25,75]
def mean(v):
    total=0
    numsum=0
    for allnum in v:
        numsum=allnum+numsum
        total=total+1
        numMean=numsum/total
    return numMean
x = mean(num)
print(x)

#With Math import

import math
num= [2,61,-1,0,88,55,3,121,25,75]
def mean(v):
    numsum=math.fsum(num)
    numMean=numsum/len(num)
    return numMean
x = mean(num)
print(x)