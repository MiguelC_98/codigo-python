#Miguel Correia

a=3
b=2
if a==5 and b>0:
    print("a is 5 and",b,"is greater than zero.")
else:
    print("a is not 5 or",b,"is not greater than zero.")

#*********************************************************

day = "Saturday"
temperature = 30
raining = False
if day=="Saturday" and temperature>20 and not raining:  
    print("Go out lazy.")
else:
    print("Better finish your python programming exercises... Or else.")
#You have to be careful using conditionals because if you use the wrong "and", "or", or "and not" it can change the whole outcome, if raining was with just "and" it would only tell you to go out when it's raining on a warm saturday, which is the opposite of what we want

#Coding with conditionals
#*********************************************************

n = 5
while n>0:
    print(n)
    n=n-1
print(n)
print("Boom!")

#A changing loop, if n=0 the loop does nothing because n has to be >0 to start the loop
#******************************************************************

n=5
while n>0:
    print("Time")
    print("Ticking")
print("Stopped")

#This is a never ending loop... Don't press play it just repeats "Time \tTicking" 
#******************************************************************

while True:
    line=input("> ")
    if line == "done":
        break
    print(line)
print("Done!")

#This loops keeps repeating itself, and the words back to you, until you say done, because when you do it causes line=done and it breaks the loop, going to whatever is next.

while True:
    line=input("> ")
    if line == "you suck":
        print("I will whoop your butt")
        continue
    if line[0] == "#":
        continue
    if line == "done":
        break
    print(line)
print("Done!")

