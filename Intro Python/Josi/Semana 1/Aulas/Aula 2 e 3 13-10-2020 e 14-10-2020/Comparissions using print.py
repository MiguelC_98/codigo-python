#Miguel Correia

print(4==5)
print(6>7)
print(15<100)
print("hello"=="hello")
print("Hello"=="hello")
print("Dog"!="Cat")
print(True==True)
print(True!=False)
print(42==42.0)
print(42=="42")
print("Apple"=="apple")
print("apple">"Apple") #Because of unicodes a>A therefore the value of apple>Apple
print("A unicode is",ord("A"),"a unicode is",ord("a")) #Each letter has a value, the ord function simply shows the value

#Showing comparissions using the print and also learning about unicode