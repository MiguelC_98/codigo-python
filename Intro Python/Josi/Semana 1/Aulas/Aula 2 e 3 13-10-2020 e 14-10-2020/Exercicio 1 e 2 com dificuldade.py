#Miguel Correia

#Exercicio 1

import random

def checknum(guess_number):
    if guess_number.isdigit() != True:
        while x!=1:
            if guess_number.isdigit()==True:
                guess_number=int(guess_number)
                return guess_number
                break
            else:
                print("Insert a valid number.")
                guess_number=input("What number am i thinking of? ")
    else:
        guess_number=int(guess_number)
        return guess_number
    
def numright(random_number,guess_number,num):
        while num != guess_number:
            checknum(guess_number)
            guess_number = int(guess_number)
            if random_number==guess_number:
                print("Oh my god how did you guess?!")
                break
            elif guess_number<random_number:
                print("Please, guess higher than {0}.".format(guess_number))
            else:
                print("Please, guess lower than {0}".format(guess_number))
            guess_number=str(input("What number am i thinking of? "))
    
difficulty=input("Choose your difficulty\n1-Easy (1-10)\n2-Medium (1-25) \n3-Hard (1-100) \n>>>")
x=0
if difficulty == "Easy" or difficulty =="1":
    num=range(10)
    guess_number=input("What number am i thinking of? ")
    checknum(guess_number)
    random_number=random.choice(num)
    numright(random_number,guess_number,num)
elif difficulty == "Medium" or difficulty == "2":
    num=range(25)
    guess_number=input("What number am i thinking of? ")
    checknum(guess_number)
    random_number=random.choice(num)
    numright(random_number,guess_number,num)
elif difficulty == "Hard" or difficulty == "3":
    num=range(100)
    guess_number=input("What number am i thinking of? ")
    checknum(guess_number)
    random_number=random.choice(num)
    numright(random_number,guess_number,num)
else:
    print("Insert a valid difficulty.")

#Exercicio 2 a

user_age=int(input("How old are you?"))
print("You are {0} years old!".format(user_age))

#Exercicio 2 b

min_workAge_Portugal=18
max_workAge_Portugal=67
if user_age>=min_workAge_Portugal and user_age<max_workAge_Portugal:
    print("Have a good day at work!")
elif user_age<min_workAge_Portugal:
    print("You are too young to work go back to school!")
else:
    print("You have worked enough, let's travel now.")
# Using the if, elif, else, and, or, and and not statements in exercises