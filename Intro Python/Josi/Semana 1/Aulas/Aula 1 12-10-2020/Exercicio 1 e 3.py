#Miguel Correia

###Exercicio 1 a

print("Welcome to python course")
print('Welcome to python course')

#It will print "Welcome to python course"


#Exercicio 1 b

print(8+5*13)

#It will print the result of 8 plus 5 multiplied by 13


#Exercicio 1 c

print("Python is fun" , 31)

#Prints on the screen "Python is fun 31"

#******************************************************************************

#Concatenating strings | Creating variables to store values | Splitting lines with \n | and using tabs \t

print("Hello" + " Miguel Correia")
print("Hello" + " " + "Miguel Correia") 
print("Hello" + " Miguel Correia" + " You are \t splitting \t spaces \n and lines")
print("Hello, Miguel Correia")
greeting = "Hello"
name = "Miguel Correia"
print(greeting , name)
print("Hello", name)
print(greeting , "Miguel Correia")
print(name)
name = 1
print(name)
name = "Miguel Correia"
print(type(name))
age = 21
print ( name , age)
print ( name + " " + str(age))
age = "21"
(name + age)
print("Hello" * 5)
print(age * 5)
print(int(age) * 5)
age = 21
print(age * 5)

#String formatting!**********************************

print('%s is %d years old' % (name, age))
print('{} is {} years old' .format(name,age))
print(f'{name} is {age} years old')

#******************************************************************

#Exercicio 3

print("Please, enter your name:")
student_name = input()
print("Welcome",student_name)
student_age = input("Please, enter your age:")
print("You are", student_age , "years old")

#Shows the use of the function "input()"