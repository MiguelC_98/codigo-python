#Exercicio 6-7

let = "abcdefghijklmnopqrstuvwxyz"
print(let[11].upper()+let[8]+let[18]+let[1]+let[14]+let[13] , let[8]+let[18] , let[8]+let[13] , let[15].upper() +let[14]+let[17]+let[19]+let[20]+let[6]+let[0]+let[11])
print()

#Writing "Lisbon is in Portugal" with the alphabet in a variable

msg="Lisbon is in Portugal"
print(msg)
print(msg[0:6],msg[7:9],msg[10:12],msg[13:21])

#Done in class  **********************

print(msg[0])   #L
print(msg[18])  #g
print(msg[4])   #o
print(msg[20])  #l
city_name= msg[0:6]   #Lisbon
print(city_name)
country_name=msg[13:21]    #Portugal
print(country_name)
print(msg[:9])   #Lisbon is

#Done in class  *********************

print(msg[1])    #i
print(msg[0:6])  #Lisbon  
print(msg[3:5])  #bo
print(msg[0:9])  #Lisbon is
print(msg[:9])   #Lisbon is

print(msg[10:14])#in P
print(msg[10:])  #in Portugal

print(msg[:6]) #Lisbon
print(msg[6:])  # is in Portugal

print(msg[:6] + msg[6:])  #Lisbon is in Portugal

print(msg[:])   #Lisbon is in Portugal

#Positive integers

print()

print(msg[-21:-15],msg[-14:-12],msg[-11:-9],msg[-8:]) #Frase escrita palavra a palavra

print(msg[-21:-15])  #Lisbon
print(msg[-8:-5])   #Por
print(msg[-14:-12])   #Is
print(msg[-8:])   #Portugal

print(msg[-11:-8])  #in
print(msg[-23:])   #Lisbon is in portugal

print(msg[-5:-1])  #tuga
print(msg[-7:])     #ortugal

print(msg[:])       #Lisbon is in Portugal

#Negative integers

#Splitting strings through their index