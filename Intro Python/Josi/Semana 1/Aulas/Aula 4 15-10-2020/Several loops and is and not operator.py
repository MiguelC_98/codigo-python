#Miguel Correia

#Simple for loop

for i in [5,4,3,2,1]:
    print(i)
print("Boom!")


#For loop with strings
friends=["Ana","Filipe","João"]
for friend in friends:
    print("Congrats on your new job,",friend+"!")
print("Done!")

#range()
print("Before")
for i in range(5):
    print(i)
print("After")

print("Before")
for i in range(2,6):
    print(i)
print("After")

print("Before")
for i in range(15,0,-5):
    print(i)
print("After")

#Looping example

print("Before")
for thing in [9,41,12,3,74,15]:
    print(thing)
print("After")

#Loop to find the average value
count=0
sum=0
print("Before",count,sum)
for value in [9,41,12,3,74,15]:
    count=count+1
    sum=sum+value
    print(count,sum,value)
average_value=sum/count
print("After",count,sum)

#Is and Not operator
smallest=None
print("Before")
for value in [3,41,12,9,74,14]:
    if smallest is None:
        smallest=value
    elif smallest>value:
        smallest=value
    print(smallest,value)
print("After",smallest)










