msg = "joseanne is a gOOd teacher"
a=msg.count("a") #counts how many of a certain character there are
print(a)
b=msg.capitalize() #transforms the first letter to an uppercase
print(b)
c=msg.casefold() #folds uppercase
print(c)
d=msg.encode() #Encodes the code
print(d)
e=msg.find("is") #Finds the spot of the first occurence of the phrase or the letter
print(e)
f=msg.isalpha() #checks if all characters are in the alphabet (spaces count as characters)
print(f)
g=msg.lower() #transforms everything to lower case
print(g)
h=msg.replace("teacher","person") #replaces phrases
print(h)
l=msg.upper() #Transforms every character with uppercase
print(l)
k=msg.translate({79:111}) #switching "O" with "o"
print(k)
msg2=["João","Miguel","Joana","Filipe"]
print(msg2)
x=" 123 ".join(msg2)  #Joins a certain string in between other strings
print(x)
m=msg.maketrans("j","s") #Creates your own translation table
print(msg.translate(m))