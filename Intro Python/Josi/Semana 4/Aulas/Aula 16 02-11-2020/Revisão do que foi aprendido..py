# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 08:37:26 2020

@author: Miguel Correia
"""
#Calculator, exercicio 1 lista 2, forma facil

n1=input("\nEnter your first number: ")
n2=input("\nEnter your second number: ")
c=input("\nEnter the operator: ")
str=n1+c+n2
print(f'\n{n1} {c} {n2}')
res=eval(str)
print("\nRES:",res)


#Calculator exercicio 1 lista 2, forma que eu fiz

while True:
    operation=input("Do you wish to add(+), subtract(-), multiply(*), or divide?(/) ")
    if operation == "add" or operation == "+" or operation == "subtract" or operation == "-" or operation == "multiply" or operation == "*" or operation == "divide" or operation == "/":
        pass
    elif operation=="quit" or operation=="Quit.":
        break
    else:
        print("Please insert a valid phrase or operator.")
        print("If you want to quit, write 'quit'.")
        continue
    while True:
        num1=input("Please insert your first number:")
        num2=input("Please insert your second number:")
        if num1.isdigit()!=True and num2.isdigit()!=True:
            print("Please insert valid numbers.")
            continue
        else:
            num1=float(num1)
            num2=float(num2)
        if operation == "+" or operation == "add":
            value=num1+num2
            print(value)
        elif operation == "-" or operation == "subtract":
            value=num1-num2
            print(value)
        elif operation == "*" or operation == "multiply":
            value=num1*num2
            print(value)
        elif operation == "/" or operation == "divide":
            if num1==0 or num2==0:
                print("Cannot divide by 0. Please insert a valid number.")
            else:
                value=num1/num2
                print("{0:.4f}".format(value))
        else:
            pass
        break
    break


#Aula 2 e 3, guess game, exercicio 1

import random

def checknum(guess_number):
    if guess_number.isdigit() != True:
        while x!=1:
            if guess_number.isdigit()==True:
                guess_number=int(guess_number)
                return guess_number
                break
            else:
                print("Insert a valid number.")
                guess_number=input("What number am i thinking of? ")
    else:
        guess_number=int(guess_number)
        return guess_number
    
def numright(random_number,guess_number,num):
        while num != guess_number:
            checknum(guess_number)
            guess_number = int(guess_number)
            if random_number==guess_number:
                print("Oh my god how did you guess?!")
                break
            elif guess_number<random_number:
                print("Please, guess higher than {0}.".format(guess_number))
            else:
                print("Please, guess lower than {0}".format(guess_number))
            guess_number=str(input("What number am i thinking of? "))
    
difficulty=input("Choose your difficulty\n1-Easy (1-10)\n2-Medium (1-25) \n3-Hard (1-100) \n>>>")
x=0
if difficulty == "Easy" or difficulty =="1":
    num=range(10)
    guess_number=input("What number am i thinking of? ")
    checknum(guess_number)
    random_number=random.choice(num)
    numright(random_number,guess_number,num)
elif difficulty == "Medium" or difficulty == "2":
    num=range(25)
    guess_number=input("What number am i thinking of? ")
    checknum(guess_number)
    random_number=random.choice(num)
    numright(random_number,guess_number,num)
elif difficulty == "Hard" or difficulty == "3":
    num=range(100)
    guess_number=input("What number am i thinking of? ")
    checknum(guess_number)
    random_number=random.choice(num)
    numright(random_number,guess_number,num)
else:
    print("Insert a valid difficulty.")


#Exercicio 3 lista 3

num={2, 61, -1, 0, 88, 55, 3, 121, 25, 75}
x25=0
x50=0
x75=0
x100=0
for x in num:
    if x<=25 and x>=0:     #can also be "if x in range(0,26):"
        x25=x25+1
    elif x<=50 and x>=26:  #can also be "if x in range(26,51):"
        x50=x50+1
    elif x<=75 and x>=51:  #can also be "if x in range(51,76):"
        x75=x75+1
    elif x<=100 and x>=76: #can also be "if x in range(76,101):"
        x100=x100+1
    else:
        pass
print("There are",x25,"numbers in between 0 and 25")
print("There are",x50,"numbers in between 25 and 50")
print("There are",x75,"numbers in between 50 and 75")
print("There are",x100,"numbers in between 75 and 100")


#Aula 2, challenge comparisson
day="Saturday"
temperature=30
raining=True

if day=="Saturday" and temperature>20 or not raining:  
    print("Go out lazy.")
else:
    print("Better finish your python programming exercises... Or else.")

"""
The problem is because the last conditional is "or not" and not 
"and not" it prints "go out" even if it isn't raining, therefore you
have to change it to and not, making it only run if it's: 
    (True and True and not False==True and True==True)
"""

"""
We learned about:
-Importing modules
-Finding function attributes
-Documentation
-Lists, several functions we can use with them, that their ordered and
mutable, therefore you can call them by index, slice them, change a 
certain index of them, you can have lists inside lists, you can use
loops to manipulate them, can concatenate them with + and *, etc.
"""
import sqlite3
from sqlite3 import Error
from numpy import *
import numpy as np

b=[4,6,7]
print(np.add(b,5))
print(dir(np))
L=[3,True,"ali",2.7,[5,8]]

a=[3,[109,27],4,15]
print(a(1)) #gives an error because it's calling a tuple
print(a[1]) #[109,27]

a=[7,5,30,2,6,25]
print(a[1:4]) #[5,30,2]

a=[1,3,6,5,3]
print(a.count(3)) #2

a=[]
for i in range(4):
    a.append(i)
print(a) #[0,1,2,3]

"""
Tuple: <indetification> = (<value1>,<value2>)
    Can use index to access or find members, therefore it's ordered
    Cannot change elements of the tuple, unmutable
    It's possible to concatenate lists with + and *
It's possible to use python built in functions to manipulate tuples,
like count() or index()
Removing any member is only possible by converting to another type of
data structure
It's possible to save tuple members in other variables by unpacking
A look into zip()
"""

t=("English","History","Mathematics")
print(t[1]) #"History"
print(t.index("English")) #0

t=(1,9,3,9)
print(t.count(9))
print(max(t))
print(t+t)

print((1,2)==(2,1)) #False

t=(4,7,2,9,8)
x=list(t)
x.remove(2)
t=tuple(x)
print(t) #(4,7,9,8)

a=(1,2)
b=(3,4)
c=zip(a,b)
x=list(c)
print(x) #[(1,3),(2,4)]
print(x[0]) #(1,3)
z=((1,3),(2,4))
u=zip(*z)
print(list(u)) #[(1,2),(3,4)]


"""
Dictionary: <identification={<key1>:<value1>,<key2>:<value2>}
Add values in the dictionary
Dictionaries have methods such as get(),pop(),popitem(),copy(),update()
It's possible to use python built in functions to manipulate dicts'
Sorting using operator module
"""
d={"brand":"cherry","model":"arizo5","color":"white"}
d["color"]="black"
print(d) #{"brand":"cherry","model":"arizo5","color":"black"}

x=d.get("model")
print(x) #arizo5

print(list(d.keys())) #["cherry","arizo5","black"]

d.pop("model")
print(d) #{"brand":"cherry","color":"black"}

num={"ali": [12,13,8],"sara":[15,7,14],"taha":[5,18,13]}
d={k: sorted(v) for k,v in num.items()}
print(d) #{"ali": [8,12,13],"sara":[7,14,15],"taha":[5,13,18]}

k=["red","green"]
v=["#FF0000","#008000"]
z=zip(k,v)
d=dict(z)
print(d) #{"red":"#FF0000","green":"#008000"}

"""
Sets <indetification>={<value1>,<value2>,<value3>}
    Add values in the sets
    they have methods such  as add(),update(),remove()
    joint theory
    subsets
"""
f={"apple","orange","banana"}
f.add("cherry")
print(f) #{'cherry', 'banana', 'orange', 'apple'}

f.update(["mango","grapes"])
print(f) #{'cherry', 'mango', 'apple', 'grapes', 'banana', 'orange'}

f.remove("apple")
print(f) #{'cherry', 'mango', 'grapes', 'banana', 'orange'}

X={1,2,3}
Y={2,3,4}
print(X.union(Y)) #{1, 2, 3, 4}
print(X|Y)        #{1, 2, 3, 4}

print(X.intersection(Y)) #{2, 3}
print(X&Y)               #{2, 3}

A={1,2,4}
B={1,2,3,4,5}
print(A.issubset(B)) #True
print(B.issubset(A)) #False

"""
Files:
    open files open(),close()
with open takes care of closing files
"""
encoding_type = "utf-8"
epopeia = open("C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Josi\\Semana 4\\Aulas\\Aula 16 02-11-2020\\Epopeia.txt","r",encoding=encoding_type)
for line in epopeia:
    print(line) #prints every line in the text
    if "tempestade" in line.lower():
        print(line) #prints the line with "tempestade" again
    else:
        pass
epopeia.close()

with open("C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Josi\\Semana 4\\Aulas\\Aula 16 02-11-2020\\Epopeia.txt","r") as epopeia:
    for line in epopeia:
        if "tempestade" in line.lower():
            print(line) #prints only two lines in the text
print(line) #if there is no text but the line is still there it will
# print an empty string.

"""
manipulating files read() readline() readlines()
readline()- reads a single line of the file and returns a string
redlines()- reads the entire file and returns a list of strings
read()- reads the entire file and returns a string
"""
file_path="C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Josi\\Semana 4\\Aulas\\Aula 16 02-11-2020\\Epopeia.txt"
with open(file_path,"r",encoding="utf-8") as epopeia:
    line = epopeia.readline()
    while line:
        print(line,end="")
        line=epopeia.readline()

file_path="C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Josi\\Semana 4\\Aulas\\Aula 16 02-11-2020\\Epopeia.txt"
with open(file_path,"r",encoding="utf-8") as epopeia:
    poem=epopeia.readlines()
    print(type(poem))
print(poem)

file_path="C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Josi\\Semana 4\\Aulas\\Aula 16 02-11-2020\\Epopeia.txt"
with open(file_path,"r",encoding="utf-8") as epopeia:
    text=epopeia.read()
    print(type(text))
print(text)

for linea in poem [::-1]:
    print(linea,end="")
print(type(poem))
"""
Because poem's type is a list it reverses the order in which the 
elements are shown, as you can see if you print this program.
"""

for linea in text[::-1]:
    print(linea,end="")
print(type(text))
"""
Because text's is a string it reverses every single character in the
text, still doing the same as the list (that is showing the last thing
first), but it reverses the order of the characters as well unlike the
list.
"""
file_path="C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Josi\\Semana 4\\Aulas\\Aula 16 02-11-2020\\Epopeia.txt"
with open(file_path,"r",encoding="utf-8") as epopeia:
    line=epopeia.readline()
    print(line)
    line=epopeia.readline()
    print(line)
    line=epopeia.readline()
    print(line)
    line=epopeia.readline()
    print(line)
    line=epopeia.readline()
    print(line)
    r=epopeia.read()
    print(r)
"""
Here we show that readline reads line by line, going to the next line
and awaiting a command in the next line, when it gets the read command
it reads the whole file starting from the last read line, because that
is where python was left when reading the file.
"""
"""
Files:
    Json files-JavaScript Object Notation file- easy to understand by
human and machines
-Data is seperated by commas
-Curly braces hold objects
Square objects hold arrays
"""
import json
file_path="C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Josi\\Semana 4\\Aulas\\Aula 16 02-11-2020\\code.json"
try:
    with open(file_path) as json_file:
        jsonObject=json.load(json_file)
except:
    print("File not found.")
else:
    name=jsonObject["data"][0]["name"] #name in the 1st object
    address=jsonObject["data"][2]["address"]["city"] #city in 3rd
    postal_code=jsonObject["data"][1]["address"]["postal_code"] 
    print(jsonObject)
    print(name)
    print(address)
    print(postal_code)

    a=json.dumps(jsonObject, indent=4)
    print(a)
finally:
    print("Something happened")
"""
pandas is usually used to access excel files.
"""
import pandas as pd
a = "C:\\Users\\migue\\Desktop\\codigo-python\\Intro Python\\Josi\\Semana 4\\Aulas\\Aula 16 02-11-2020\\Employee_data.xlsx"
all_data = pd.read_excel(a,index_col=1)
email=all_data["email"].head()
company_name=all_data["company_name"]
xl=pd.ExcelFile(a)
df=xl.parse("b")

"""
Lambda functions also called annonymous functions
Can recieve any number of arguments
lambda <arguments>:<expression>
"""
def power_three(x):
    return (x**3)
print(power_three(10)) #1000
a= lambda x:x**3
print(a(10)) #1000

def Larger_num(num1,num2):
    if num1>num2:
        return num1
    else:
        return num2
print(Larger_num(5,7)) #7
larger_num= lambda num1,num2:num1 if num1>num2 else num2
print(larger_num(5,7)) #7

"""
Maps perform an operation and return its value in a list
"""
text="LISBON IS IN PORTUGAL"
def char_lowercase():
    char_lowercase=[char.lower() for char in text]
    return char_lowercase
def map_lowercase():
    map_lowercase=list(map(str.lower,text))
    return map_lowercase

def comp_words():
    words_lowercase=[word.lower() for word in text.split(" ")]
    return words_lowercase
def map_words():
    map_w=list(map(str.lower,text.split(" ")))
    return map_w
print(char_lowercase()) #['l', 'i', 's', 'b', 'o', 'n',...]
print(map_lowercase())  #['l', 'i', 's', 'b', 'o', 'n',...]
print(comp_words()) #['lisbon', 'is', 'in', 'portugal']
print(map_words())  #['lisbon', 'is', 'in', 'portugal']

"""
Filter, performs an operation and return its value in a list
"""
vegetables=[
    ["Beets","Cauliflower","Broccoli"],
    ["Beets","Carrot","Cauliflower"],
    ["Beets","Carrot"],
    ["Beets","Broccoli","Carrot"]
    ]
def not_brocoli(food_list: list):
    return "Broccoli" not in food_list
brocoli_less_food=list(filter(not_brocoli,vegetables))
print(brocoli_less_food) #[['Beets', 'Carrot', 'Cauliflower'], ['Beets', 'Carrot']]

"""
Classes:
    Object oriented Programming
    Aims to combine data and processes related to that data (objects)
    
    The __init__() method is called a constructor method
    The increase() method increments the value of the attribute val
by 1
    The show() method displays the value of the attribute val
"""
class josi:
    a=[2,4,6]
    b=[7,8]
    c=[10,11,21]
    def __init__(self,val):
        self.val=val
        print(val)
    def increase(self):
        self.val+=1
    def show(self):
        print(self.val)

x=josi(3) #val=3
c=x.increase() #val=4
d=x.show()
print(type(x.a)) #<class 'list'>
print(x.a.__add__(x.b)) #[2,4,6,7,8]
print(x.a.__eq__(x.b)) #False, if a and b are equal
print(x.a.__ne__(x.c)) #True, if a and c are not equal















