# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 08:30:40 2020

@author: Miguel Correia
"""

"""
What's Numpy?
It's a fundamental package needed for scientific conputing with Python
Some of its feautures are:
    -Typed multidimensional arrays (vectors and matrices)
    -Fast numerical computations (matrix math)
    -High-level math functions
    -Open Source
Numpy's code is way better optimized than Python's code in mathematical
functions, being way faster and using less memory to do so.
"""
import numpy as np
print(np.__version__) #1.19.2
print(np.version) #module 'numpy version' from 'file-path'

np.array([1,2,3]) #creates an array [1,2,3]
#array([1,2,3]) #gives an error

students = np.array([[[1,3,5],[1,1,1]],
                    [[4.5,4,5],[4.3,4.4,4.6]]],dtype=int)

print(students)
print(students.shape,"\n", #(2,2,3)
      students.nbytes,"\n", #96 occupies 96 bytes
      students.ndim,"\n", #3  it's in 3 dimensions
      students.dtype,"\n", #float64 (data type) float 64 bits, then changed to int32
      students.size,"\n", #12 == 2*2*3
      students.data,"\n", #where it's located in the memory
      students.itemsize) #length of an array in bytes


#CALCULATIONS WITH ARRAYS

x = np.array([[-1,3],[4,2]])
y = np.array([[1,2],[3,4]])
z = np.dot(x,y)
print(z)


import numpy as np
x3 = np.power(10,4,dtype=np.int8) #10^4==10000
print(x3) #prints 16 because the variable couldn't store it's actual value
x4 = np.power(10,4,dtype=np.int32) #10000
print(x4)
x5 = np.power(10,4,dtype=np.int16) #this is the most optimized one
print(x5)

"""
We have to have in consideration the bits that we use as to not use more
memory than needed when dealing with the numbers, therefore optimizing
the resource use.
"""

import numpy as np
x1=np.array([[-1,3]])
x2=np.array([[1,2]])
x3=x1+x2
x4=x1*x2
x5=x1-x2
print(x1) #[[-1 3]]
print(x2) #[[1 2]]
print(x3) #[[0 5]] -1+1 3+2
print(x4) #[[-1 6]] -1*1 3*2
print(x5) #[[-2 1]] -1-1 3-2

"""
Array broadcasting, ver o powerpoint
"""
import numpy as np
print(np.arange(10)) #[0 1 2... 8 9]
print(np.arange(10).reshape(2,5)) #[[0 1 2 3 4]
                                  # [5 6 7 8 9]]
#You can't have the reshape(2,7) because it's bigger than the array size
print((np.resize(np.arange(10),(2,5)))) #[[0 1 2 3 4]
                                         #[5 6 7 8 9]]
print((np.resize(np.arange(10),(2,7))))#[[0 1 2 3 4 5 6]
                                        #[7 8 9 0 1 2 3]]
print((np.resize(np.arange(30),(3,20)))) #prints a lot of numbers
                                         #copying to fit the whole thing
"""
np.resize copies the numbers if there aren't enough numbers to fit,
like we can see in the example above
"""
import numpy as np
d = np.arange(2,5) #[2 3 4]
print(np.arange(2,5)) #[2 3 4]
print(d.shape)        #(3,) 3 items, without any other lines, or blocks
print(d[:, np.newaxis])#[[2]
                       # [3]
                       # [4]]
print(d[:, np.newaxis].shape) #(3,1) #3 items in 1 block each

import numpy as np
students1=np.array([[1,3,5]])
students2=np.array([[[1,3,5],[1,1,1]]])
students3 = np.array([[[1,3,5],[1,1,1]],
                    [[4,4,5],[1,2,3]]])
print(students1.shape) #(1,3) 1 item per 3 blocks
print(students2.shape) #(1,2,3) 1 item, per two data sets, in 3 blocks
print(students3.shape) #(2,2,3)


import numpy as np
x=np.array([1,4,3])
y=x #copies x but changes if x changes
z=np.copy(x) #copies x, not changing if x changes
x[1]=2 #changes the second item from 4 to 2
print(x) #[1 2 3]
print(y) #[1 2 3]
print(z) #[1 4 3]

"""
Sorting: organizing an array using a specific parameter
ver pq e que o print tem b' antes (tem a ver com o encoding)
VER O POWERPOINT
"""
import numpy as np
dtype=[("name","U12"),("grade",float),("age",int)]
values=[("Joseanne",5,31),("Hamed",5,32),("Stefan",5,24)]
sorted_data=np.array(values,dtype=dtype)
print(sorted_data) #goes from the first value ("Joseanne") to the last
print(np.sort(sorted_data,order="age")) #sorts by whose age is lowest
print(sorted_data.shape)


import numpy as np
b=np.zeros((10,6),dtype=int)
c=np.zeros((2,2,3),dtype=float)
print(b.ndim, b.dtype,b.size) # 2 dimensions, int32 type, 60 bytes
print(c.ndim,c.dtype,c.size) #3 dimensions,float64 type, 12 bytes
d=np.zeros((10,10))
print("%d bytes"%(d.size*d.itemsize)) #800 bytes, that's how many the
                                      #array of 0's is using
e=np.zeros(10) 
e[8]=1
print(e.dtype) #float64, it's the standart np.zeros d.type
print(e) #[0. 0. 0. 0. 0. 0. 0. 0. 1. 0.]

#SLICING ARRAYS
f=np.arange(10,50)
print(f)
print(f.ndim,f.dtype,f.size)
g=np.arange(50)
g=g[::-1] #reverse it
g=g.reshape(5,10)
print(g)
print(g[4,1]) #8, 4th row, 2nd item in index
print(g[3,:]) #3rd row
print(g[0,2:]) #1st row, starting from 3rd element
print(g[:1,1:]) #prints until the first row, therefore only printing it
                #from the 2nd element

"""
x[0,0] #top left element
x[0,-1] #first row, last column
x[0,:] #first row (all of it's entries)
x[:,0]
"""

#Concatenating arrays
print(np.ones((2,1))) #same as np.zeros but ones
b=np.arange(50)
b=b.reshape(5,10)
c=np.ones((2,10))
print(np.concatenate((b,c))) #adds c's rows to b, starting from its
                             #final row
d=np.hstack(c)
print(np.vstack(d)) #transforms it to one giant column
print(np.hstack(d)) #transforms it to one giant row
"""
VER O POWERPOINT PARA VSTACK
"""

#astype()
b=np.arange(50)
c=b.astype(np.int16) #changes the dtype from int32 to int16
print(b.astype(np.int16)) #prints c and not b because dtype=int16
print(b.ndim,b.dtype,b.size) #1 int32 50
print(c.ndim,c.dtype,c.size) #1 int16 59

#zeroslike
d=np.ones((1,3))
x=np.zeros_like((d)) #transforms it to all zeros but with the format of d
x1=np.ones_like((x)) #transforms it to all ones but with the format of x
print(d) #[[1. 1. 1.]]
print(x) #[[0. 0. 0.]]
print(x1) #[[1. 1. 1.]]
b=np.eye(3)
print(b)#[[1. 0. 0.]
        #[0. 1. 0.]
        #[0. 0. 1.]]
print(np.empty((1,3))) #creates an array with the lowest value possible
a=np.full((2,1),np.inf)
print(a) #[[inf]
         #[inf]]
print("%d bytes"%(a.size*a.itemsize)) #16 bytes, even though it's inf

"""
Ver o powerpoint 
"""

b=np.random.random((10,10))
bmin,bmax=b.min(),b.max()
print(bmin,bmax)


yesterday=np.datetime64("today") - np.timedelta64(1)
today=np.datetime64("today")
tomorrow=np.datetime64("today") + np.timedelta64(1)
print(yesterday,today,tomorrow)
b=np.arange("2019","2021",dtype="datetime64[D]")
print(b.size,b.ndim) #731 1, 731 days the two years have, days being 
                     #only one dimension
"""
by doing the np.arange("2019","2021",dtype="datetime64[D]") we can 
change the year, specify the month in the intervals, or count by 
months
"""

import matplotlib.pyplot as plt
values = np.linspace(-(2*np.pi),2*np.pi,100) #circulo trignometrico
cos_values=np.cos(values)
sin_values=np.sin(values)
plt.plot(cos_values,color="blue",marker=".")
plt.plot(sin_values,color="red")
plt.show()

"""
Ver powerpoint para import de imagens
"""

















