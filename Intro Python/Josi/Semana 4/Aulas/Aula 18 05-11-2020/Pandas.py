# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 08:40:00 2020

@author: Miguel Correia
"""

import pandas as pd
import numpy as np

print(dir(pd))
print(pd.__version__) #1.1.3

data = pd.Series((0.25,0.5,0.75,1.0))
print(data)
print(data.index)
print(data.values)
print(data[0])
print(data[1:3])

#Dá para mudar o index a partir do panda!
data=pd.Series((0.25,0.5,0.75,1.0),index=["a","b","c","d"])
print(data)


data=pd.Series((0.25,0.5,0.75,1.0),index=[2,5,3,7])
print(data)
print(data[5])

population_dict={"California":38332521,
                 "Texas":26448193,
                 "New York":198651127,
                 "Florida":19552860,
                 "Illiniois":14999543}
population_dict=pd.Series(population_dict)
print(population_dict)
print(population_dict["California"])
print(population_dict["California":"New York"])


obj=pd.Series([2,4,6])
print(obj)


obj=pd.Series(5,index=[100,200,300])
print(obj)


obj=pd.Series({2:"a",1:"b",3:"c"},index=[3,2])
print(obj)


area_dict={"California":423967,
                 "Texas":695662,
                 "New York":141297,
                 "Florida":154392,
                 "Illiniois":149995}
area=pd.Series(area_dict)
print(area)

state=pd.DataFrame({"population":population_dict,"area":area})
print(state)
print(state["area"])


print(pd.DataFrame(population_dict,columns=["population"]))

data=[{"a":1,"b":2*i}
      for i in range(3)]
print(pd.DataFrame(data))


print(np.log(10))
print(pd.DataFrame([{"a":1,"b":2},{"b":3,"c":4}]))

A=np.zeros(3,dtype=[("A","i8"),("B","f8")])
print(A)
print(pd.DataFrame(A))

ind=pd.Index([2,3,5,7,11])
print(ind)
print(ind[1])
indA=pd.Index([1,3,5,7,9])
indB=pd.Index([2,3,5,7,11])
print(ind[::2])
print(ind.size,ind.shape,ind.ndim,ind.dtype)
#ind[1]=0 doesn't work because index object is immutable
print(indA & indB) #interssection
print(indA | indB) #reunion
print(indA ^ indB) #sym difference


















