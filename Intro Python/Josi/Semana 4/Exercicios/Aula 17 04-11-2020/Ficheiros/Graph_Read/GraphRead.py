# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 11:14:05 2020

@author: Miguel Correia
"""
import pathlib
import csv
def get_files(country):
    all_list=[]
    file_path=str(pathlib.Path().absolute())+"\\DP_LIVE_04112020161547500.csv"
    with open(file_path) as file:
        reader=csv.reader(file,delimiter=",")
        for row in reader:
            if country in row:
                all_list.append(row)
    return all_list

def lists_years_score(c_list,gender):
    c_boys_score=[]
    c_boys_year=[]
    for lists in c_list:
        lists[6]=float(lists[6])
        if lists[2]==gender:
            c_boys_score.append(lists[6])
            c_boys_year.append(lists[5])
    return c_boys_score,c_boys_year