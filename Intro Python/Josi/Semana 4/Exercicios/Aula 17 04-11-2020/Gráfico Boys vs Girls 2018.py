# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 11:16:59 2020

@author: Miguel Correia
"""
import matplotlib.pyplot as plt
import GraphRead

all_list=GraphRead.get_files("PRT")
port_list_boys=[]
port_list_girls=[]
for lists in all_list:
    if  lists[2]=="BOY":
        port_list_boys.append(lists)
    elif lists[2]=="GIRL":
        port_list_girls.append(lists)
list_med_2018=[int(port_list_boys[5][6]),int(port_list_girls[5][6])]
list_gender_2018=[1,2]
bar_list=plt.bar(list_gender_2018,list_med_2018,width=0.6,align="center")
bar_list[1].set_color("pink")
plt.ylim(450,505)
plt.xticks(list_gender_2018,["Boys","Girls"])
plt.ylabel("Meanscore Values")
plt.title("Meanscore Portugal Gender comparisson\n2018")
plt.show()