# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 13:11:21 2020

@author: Miguel Correia

Input:
    Dados do ficheiro
Output:
    Gráfico de barras medindo o desempenho de as raparigas e rapazes, em
    separado, mediano, de Portugal em relação a outros paises

Step 1:
    Buscar os dados ao ficheiro||||||||DONE
Step 2:
    Criar um array com os dados necessários ||||||||||| DONE
Step 3:
    Mostrar os dados num gráfico de barras, bem organizado :)
"""

import matplotlib.pyplot as plt
import GraphRead

p_list=GraphRead.get_files("PRT")
k_list=GraphRead.get_files("KOR")
j_list=GraphRead.get_files("JPN")
g_list=GraphRead.get_files("GRC")
e_list=GraphRead.get_files("ESP")
s_list=GraphRead.get_files("SWE")
p_boys_score,p_boys_year=GraphRead.lists_years_score(p_list,"BOY")
k_boys_score,k_boys_year=GraphRead.lists_years_score(k_list,"BOY")
j_boys_score,j_boys_year=GraphRead.lists_years_score(j_list,"BOY")
g_boys_score,g_boys_year=GraphRead.lists_years_score(g_list,"BOY")
e_boys_score,e_boys_year=GraphRead.lists_years_score(e_list,"BOY")
s_boys_score,s_boys_year=GraphRead.lists_years_score(s_list,"BOY")
plt.style.use("fivethirtyeight")
plt.plot(p_boys_year,p_boys_score,marker=".",label="Portugal",color="red")
plt.plot(k_boys_year,k_boys_score,marker=".",label="Korea",color="cyan")
plt.plot(j_boys_year,j_boys_score,marker=".",label="Japan",color="orange")
plt.plot(g_boys_year,g_boys_score,marker=".",label="Greece",color="blue")
plt.plot(e_boys_year,e_boys_score,marker=".",label="Spain",color="yellow")
plt.plot(s_boys_year,s_boys_score,marker=".",label="Sweden",color="purple")
plt.ylim(300,600)
plt.ylabel("Meanscore Values")
plt.xlabel("Years")
plt.title("Mathematics Meanscore\nper year")
plt.legend()
























