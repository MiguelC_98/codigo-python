# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 11:27:03 2020

@author: Miguel Correia
"""
import matplotlib.pyplot as plt
import GraphRead

port_list=GraphRead.get_files("PRT")
port_boys_score,port_boys_year=GraphRead.lists_years_score(port_list,"BOY")
plt.style.use("Solarize_Light2")
plt.plot(port_boys_year,port_boys_score,marker=".",color="#7790E7",label="Portugal")
plt.ylim(430,530)
plt.ylabel("Meanscore Values")
plt.xlabel("Years")
plt.title("Mathematics Meanscore,per year\nPortugal")
plt.legend()