# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 16:34:10 2020

@author: Miguel Correia
"""
import math
def squareArea(x):
    return (x*x)
def squarePerimeter(x):
    return (x*4)
def retangulArea(h,b):
    return (h*b)

def spherePerimeter(r):
    return (2*math.pi*r)
def sphereArea(r):
    return (math.pi*(r**2))
def sphereVol(r):
    return ((4/3)*math.pi*(r**3))
