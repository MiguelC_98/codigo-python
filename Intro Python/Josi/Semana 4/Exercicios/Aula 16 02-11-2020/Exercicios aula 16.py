# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 16:50:53 2020

@author: Miguel Correia
"""

#Exercicio 1
import GeometryCalculations as GC
try:
    length=int(input("Please, give me a number: "))
    print("Here is the sphere perimeter: ",GC.spherePerimeter(length))
    print("Here is the square perimeter: ",GC.squarePerimeter(length))
    print("Here is the sphere area: ",GC.sphereArea(length))
    print("Here is the square area: ",GC.squareArea(length))
except:
    print("A number, please... Not whatever you put...")


#Exercicio 2
def list_num():
    num_list=[]
    while True:
        num=input("Insert a number to the list, if you are done write 'done': ")
        if num=="done":
            break
        else:
            try:
                num=int(num)
                num_list.append(num)
            except:
                print("Please insert a valid number.")
    return num_list
def frequencies(v):
    nums={}
    for n in v:
        if n not in nums:
            nums[n]=1
        else:
            nums[n]+=1
    high_freq=0
    low_freq=len(v)
    for t in nums.values():
        if t>high_freq:
            high_freq=t
        elif t<low_freq:
            low_freq=t
        else:
            pass
    f1=[]
    f2=[]
    for t in nums.items():
        if t[1]==low_freq:
            f1.append(t[0])
        elif t[1]==high_freq:
            f2.append(t[0])
        else:
            pass
    return (f1,f2)
#if you want to add the list yourself
v=list_num()
#The actual exercise list
v=[1,4,5,1,6,3,2,1,2,9,1,4,6,3,9,7]
print(frequencies(v))