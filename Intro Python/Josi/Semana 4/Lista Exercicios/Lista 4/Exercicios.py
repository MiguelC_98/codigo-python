# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 11:57:42 2020

@author: Miguel Correia
"""

#Exercicio 1
"""
experimentar livrar de grande parte de isto e ver as possiblidades de
número em todos os zeros, mete-los num dicionario em que as keys sao
o row,col(index de row e index de col em tuple) e os values sao uma 
lista de numeros possiveis para o espaço, se houver algum número numa
lista em que não esteja na lista de outro número da mesma row, col ou
caixa, entao o número é valido.

JÁ ESTÁ RESOLVIDO
"""
import numpy as np
import sys
sudoku=np.array([[0,2,0,5,0,1,0,9,0],
                 [8,0,0,2,0,3,0,0,6],
                 [0,3,0,0,6,0,0,7,0],
                 [0,0,1,0,0,0,6,0,0],
                 [5,4,0,0,0,0,0,1,9],
                 [0,0,2,0,0,0,7,0,0],
                 [0,9,0,0,3,0,0,8,0],
                 [2,0,0,8,0,4,0,0,7],
                 [0,1,0,9,0,7,0,6,0]])
def print_board(sudoku):
    """ Shows the sudoku board in the console """
    for i in range(len(sudoku)):
        if i%3==0 and i!=0:
            print("- - - - - - - - - - - - - ")
        for j in range(len(sudoku[0])):
            if j%3==0 and j!=0:
                print(" | ", end="")
            if j==8:
                print(sudoku[i][j])
            else:
                print(str(sudoku[i][j]) + " ", end="")

def ValuesAdd(y,x,num):
    global sudoku
    box_x=(x//3)*3
    box_y=(y//3)*3
    totalbox=0
    totalrow=0
    totalcol=0
    #Counting the sum of the box numbers
    for i in range(box_y,box_y+3):
        for j in range(box_x,box_x+3):
            totalbox+=sudoku[i][j]
    #Counting the sum of the column's numbers
    for i in range(len(sudoku[0])):
        totalrow+=sudoku[y][i]
    #Counting the sum of the row's numbers
    for i in range(len(sudoku)):
        totalcol+=sudoku[i][x]
    #Checking if the numbers are higher than what they can be
    if totalbox>45 or totalrow>45 or totalcol>45:
        return False
    else:
        return True

def possible(y,x,num):
    """checks to see if it's possible to insert a certain number(num) in a certain space(x,y) """
    global sudoku
    for i in range(0,9): #goes through the grids index
        if sudoku[y][i]==num: #checks to see if any row num is equal to this one
            return False
        if sudoku[i][x]==num: #checks to see if any column num is equal to this one
            return False
    box_y=(y//3)*3 #value to go through the num's box
    box_x=(x//3)*3 #value to go through the num's box
    for i in range(0,3):
        for j in range(0,3):
            if sudoku[box_y+i][box_x+i]==num: #checks to see if there isn't an equal num in the box
                return False #if there is, returns false
    if ValuesAdd(y,x,num)==False: #checks the sum of the nums in row, column
        return False #and box, if they're bigger than 45 return false
    return True
    
def isSolution(sudoku):
    for y in range(9): 
        for x in range(9):
            if sudoku[y][x]==0: #checks which spaces are empty
                for num in range(1,10): #tries giving numbers to sudoku
                    if possible(y,x,num)==True:
                        sudoku[y][x]=num #inserts the number
                        isSolution(sudoku) #goes back to the beginning, checking for more empty spaces
                        sudoku[y][x]=0 #if there is a wrong number in a place, goes back and retries
                return 
    print_board(sudoku)
    print("-------------------------")
    print("Sudoku is solved!")
    sys.exit("Sudoku is solved!")
isSolution(sudoku)

#Exercicio 2
j=1 #GLOBAL VARIABLE
def main():
    a=9 #LOCAL VARIABLE
    
    if a%2==0:
        a=2 #LOCAL VARIABLE
    else:
        a=3 #LOCAL VARIABLE
    print(fun1(2,4))
    for i in range(3):
        for j in range(3):
            print(fun1(a,i+j))
def fun1(a,b):
    p=1 #LOCAL VARIABLE
    for i in range(b):
        p=p*a #LOCAL VARIABLE
    return p+j
main()
"""
a)
    Local Variables: a,i,j,p,b
    Global variables: j
-j is first a global variable, belonging to all of the program, then it's
used as a local variable in main(), therefore when it's used in
"return p+j" it only adds 1 to p
-a belongs to main() and fun1(), but it's different in fun1()
-b belongs to fun1()
-i belongs to main() and fun() being used to store range values
-p belongs to fun1()

b)
17 #Is what's printed right before the for loops in main()
2  #first print in the for loops, i=0 and j=0, therefore b=0 and p+j=2
4  #i=0,j=1,b=1,p=3,p+j=4
10 #i=0,j=2,b=2,p=9,p+j=10
4  #i=1,j=0,...
10 #i=1,j=1,...
28 #i=1,j=2,...
10 #i=2,j=0,...
28 #i=2,j=1,...
82 #i=2,j=2,...
"""
#Exercicio 3

def list_num():
    num_list=[]
    while True:
        num=input("Insert a number to the list, if you are done write 'done': ")
        if num=="done":
            break
        else:
            try:
                num=int(num)
                num_list.append(num)
            except:
                print("Please insert a valid number.")
    return num_list
num_list=list_num()
try:
    A=int(input("Insert a number: "))
except:
    print("A number, please.")
imp_nums=()
for i in num_list:
    for j in num_list:
        num=i*j
        if num==A:
            imp_nums=(i,j)
        else:
            continue
if imp_nums!=():
    print(A,"is equal to",imp_nums[0],"*",imp_nums[1])
else:
    print(A,"is not equal to the multiplication of any numbers in the list")




















